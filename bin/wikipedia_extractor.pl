#!/usr/bin/perl

use locale;
#use Encode;
use utf8;
binmode STDIN, ":utf8";
binmode STDOUT, ":utf8";
binmode STDERR, ":utf8";

my $nomp_lex_file = "";

my $first = 0;
my $lang = "fr";
my $extract_variants_from_first_line = 0;

while (1) {
  $_ = shift;
  last if (!defined($_));
  if (/^$/) {last;}
  elsif (/^-f$/) {$first = shift;}
  elsif (/^-l$/) {$lang = shift;}
  elsif (/^-vffl$/) {$extract_variants_from_first_line = 1;}
  else {die "Unknown option: $_";}
}

open REDIRECT, ">${lang}wiki.redirections" || die "Could not open redirections";
binmode REDIRECT, ":utf8";

my ($def,$title,$id);

while (<>) {
  $l++;
  print STDERR "\t articles...\t\tline $l  \r"  if ($l % 1000 == 0);
  $skip = 0;
  chomp;
  if (/^<mediawiki.*lang=\"(..)\">/) {$lang = $1;}
  $orig_line = $_;
  alarm 0;
  eval {
    local $SIG{ALRM} = sub { die "timeout\n" };
    alarm 4; # So we can NEVER use "next;" for the while loop, or that alarm stuff may screw up (hence the use of $skip)

    if (/<title>(.*)<\/title>/) {
      $art++;
      $art_length = 0;
      if ($art >= $first) {
	print STDERR "Studying articles...$art   \r";
#	%hash=();
	$title = $1;
	if ($title =~ /:[^ ]/) {
	  $title = "";
	}
	$qmtitle = quotemeta($title);
	$skip = 0;
      } else {
	print STDERR "Skipping articles...$art   \r";
	$skip = 1;
      }
    } elsif (/<id>(.*?)<\/id>/ && $id eq "") {
      $id = $1;
    } else {
      $art_length++;
      if (!$skip && $art >= $first) {
	if (/<\/page>/) {
	  if ($title ne "") {
	    print "${lang}wiki$id\t$title\t$art_length\t".(join("|",sort keys %cats))."\t$def\n";
	  }
	  $id = "";
	  $def = "";
	  $bool = 0;
	  $skip = 1;
	  $in_open_brackets = 0;
	  $beginning = "";
	  %cats = ();
	  #      print "$title\t$odef\t$def\n" unless $title eq "";
	} elsif ($def eq "" && s/^.*<text.*?>(?:&lt;references\/&gt;)?//) {
	  $bool = 1;
	  $in_open_brackets = 0;
	  $beginning = "";
	} elsif (   ($lang eq "fr" && /^\[\[Catégorie:\s*(.*?)\s*(\]\]|\|)/i)
		 || ($lang eq "en" && /^\[\[Category:\s*(.*?)\s*(\]\]|\|)/i)) {
	  $cats{$1} = 1 unless length($1) <= 1;
	}
      } else {
	$skip = 1;
      }
    }
    if ($skip == 0) {
      s/\[\[\]\]//g;
      if (/\{\{[^\{\}]*(škrbin|stub|ébauche)/) {
	s/^(.*?)\{\{[^\{\}]*(?:škrbin|stub|ébauche).*?\}\}/\1/; # capturer et remettre \1 ne sert à
                                                                # rien, mais empêche une boucle
                                                                # infinie sur un cas particulier du
                                                                # dump wikipedia français téléchargé
                                                                # début février 2010
      }
      if (/\#redirect \[\[(.*?)\]\]/i) {
	$skip = 1;
	print REDIRECT "$title\t$1\n" unless $title eq "";
	$title = "";
      } elsif (/\{\{homonym(?:ie|y)\}\}/) {
	$title = "";
	$skip = 1;
      } elsif ($bool && $title ne "" && $def eq "") {
	$_ = $beginning.$_;
	$beginning = "";
	if (/^\[\[(Image|Slika|Soubor):/) {
	  while (s/^\[\[[^\[\]]*\[\[[^\[\]]*\]\]/\[\[/) {}
	  while (s/^\[\[[^\[]*\[[^\[\]]*\]/\[\[/) {}
	  s/^\[\[[^\]]+\]\]+//;
	  s/^[^\[\]]*\]\]+//;
	}
	if (/^\s*$/
	    || /^:/
	    || /Voir Homonymes/i
	    || /\{\{redirect/i
	    || /^\&lt;!--/) {
	  $skip = 1; # inutile mais bon...
	} else {
	  if (/^\s*\{\{[^\}]*$/) {
	    $in_open_brackets++;
	    $skip = 1;
	  } else {
	    if ($in_open_brackets > 0) {
	      if (/^[^\{]*\}\}/) {
		$in_open_brackets = 0;
	      }
	      $skip = 1;
	    } elsif (/^\s*(?:\{\{[^\{\}]*\}\})?$/) {
	      $skip = 1;
	    }
	  }
	  if (!$skip && !$in_open_brackets && (/..\[[^\]]*$/ || /..\{[^\}]*$/)) {
	    $beginning = $_;
	    $skip = 1;
	  }
	  if (!$skip) {
	    s/.lt;ref.*?gt.*?lt.\/ref.gt.//g;
	    s/(av|st|St)\./$1\__DOT__/g;
	    while (s/(\[\[[^\]]*)\./$1\__DOT__/g) {}
	    while (s/(\{\{[^\}]*)\./$1\__DOT__/g) {}
	    $def = $_;
	    $def =~ s/^(.*?[^ ][^ ])\. *(?: ([A-Z]|É|À|È|Ç|Č|Š|Ž|Ň|Ť|Ď|Ľ|Á|Í|Ó|Ú|Ý).*|$)/\1/;
	    $def =~ s/__DOT__/./g;
	    $odef = $def;
	    $def =~ s/\s*&lt;\s*br\s*&gt;\s*/ /g;
	    $def =~ s/  +/ /g;
	    $def =~ s/^ //;
	    $def =~ s/’/'/g;
	    $def =~ s/{{\!}}//g;
	    $def =~ s/{{PAGENAME,?}}+/PAGENAME/g;
	    $def =~ s/\{[\{\[]date\|(.*?)\|(.*?)\|(.*?)\}\}/\1 \2 \3/gi;
	    $def =~ s/\{[\{\[]linktext.*?\}\}//gi;
	    $def =~ s/\{[\{\[]formatnum:(.*?)\}\}/\1/gi;
	    $def =~ s/\{[\{\[]pron[ou]nciation\|(.*?)\}\}//gi;
	    $def =~ s/\{[\{\[]IPA\|(.*?)\}\}//gi;
	    $def =~ s/\{[\{\[][a-z ]+\|([^|{}]+)\|.*?\}\}/\1/gi;
	    $def =~ s/\{[\{\[]lang-[a-z]+\|([^|{}]+)\}\}/\1/gi;
	    $def =~ s/\{[\{\[]zh-[a-z]+\|.*?\|p=[^\|]*\}\}//gi;
	    $def =~ s/[\(\（][^\(\)\（\）]*?[\)\）]//g;
	    $def =~ s/[\(\（][^\(\)\（\）]*?[\)\）]//g;
	    $def =~ s/&lt;!--.*?--&gt;//g;
	    if ($def =~ /&lt;small&gt;/ && $def =~ /&lt;\/small&gt;/) { # sinon on boucle...
	      $def =~ s/&lt;small&gt;.*?&lt;\/small&gt;//g;
	    }
	    $def =~ s/, ,/,/g;
	    $def =~ s/''' '''/ /g;
	    $def =~ s/'''  '''/ /g;
	    $def =~ s/'''   '''/ /g; # avec " +", ça boucle sur une phrase, sans aucune raison apparente...
	    $def =~ s/\&amp;/\&/g;
	    $def =~ s/\&nbsp;/ /g;
	    $def =~ s/  +/ /g;
	    $def =~ s/^ //;
	    $def =~ s/ $//;
	    $def =~ s/\[\[([^\|\[\]]*)\](?!\])/$1/g; # nettoyage...
	    $def =~ s/(?<!\[)\[([^\|\[\]]*)\]\]/$1/g; # nettoyage...
	    if ($def=~/^\s*$/ || $def =~ /^\s*\*/ || $def =~ /^\[\[\]\]$/) {
	      $skip = 1;
	    } else {
	      $skip = 1 unless $def =~ s/^(?:|.*?(?:[ \[]|[Ll]'))'''+(?!')//;
	      if (!$skip) {
		$def =~ s/^([^\[]+)'''([^\[\]]*)\]\]/$1$2/;
		while ($def =~ s/^(.*?\[\[[^\]]*)'''([^\[]+?)'''([^\[\]]*\]\])/$1$2$3$4/g) {}
		$def =~ s/^([^'](?:.*[^'])?)'''+(?:\&quot;|,|:)? //;
	      }
	    }
	  }
	  if ($def=~/^\s*$/ || $def =~ /^\s*\*/ || $def =~ /^\[\[\]\]$/) {
	    $skip = 1;
	  }
	  if (!$skip) {
	    $def =~ s/^, +//;
	    $def =~ s/^ *[\(\（] *//;
	    $def =~ s/^[^\{\}]*(?:\{\{[^\{\}]*\}\}[^\{\}]*)?\}\} *//;
	    $def =~ s/^[^\[\]]*(?:\[\[[^\[\]]*\]\][^\[\]]*)?\]\] *//;
	    $def =~ s/–/-/g;
	    $def = clean ($def);
	    $bool = 0;
	  } else {
#	    $def = clean ($def);
	    $def = "";
	  }
	}
      }
    }
  };
  if ($@ and $@ =~ /timeout/) {
    print STDERR "Studying articles...\t\tline $l\t(screwed up)\t$title\t$orig_line\n";      
    $title = "";
    $def = "";
  }
  alarm 0;
}

close REDIRECT;

print STDERR "\ndone\n";

sub clean {
  my $s = shift;
  $s =~ s/\[\[(.[^\|]*?)\]\]/$1/g;
  $s =~ s/\[\[.*?\|(.*?)\]\]/$1/g;
  $s =~ s/\{\{(.[^\|]*?)\}\}/$1/g;
  $s =~ s/\{\{.*?\|(.*?)\}\}/$1/g;
  $s =~ s/\'\'+//g;
  $s =~ s/\&quot;/'/g;
  $s =~ s/  +/ /g;
  $s =~ s/ $//;
  $s =~ s/(, )+/, /g;
  $s =~ s/, $//g;
  $s =~ s/ ,/,/g;
  return $s;
}

