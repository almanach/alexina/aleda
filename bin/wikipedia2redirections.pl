#!/usr/bin/perl

use utf8;
binmode STDIN, ":utf8";
binmode STDOUT, ":utf8";
binmode STDERR, ":utf8";

$sourceid = shift || die;

while (<>) {
  chomp;
  if (/^\s*<title>(.*?)<\/title>/) {
    $title = $1;
    $id = 0;
  } elsif ($id == 0 && /^\s*<id>(\d+)<\/id>/) {
    $id = $1;
    $title2id{$title} = $id;
  } elsif (/^\s*<\/page>/) {
    $id = -1;
    $title = "";
  } elsif (/#redirect\S*\s*\[\[(.*?)\]\]/i) {
    if ($title ne "") {
      $redir{$title} = $1;
    }
  }
}

for (keys %redir) {
  unless (defined($printed{$_}{$title2id{$redir{$_}}})) {
    print "$_\t$sourceid$title2id{$redir{$_}}\t$redir{$_}\n" if (defined $title2id{$redir{$_}});
    $printed{$_}{$title2id{$redir{$_}}} = 1;
  }
}
