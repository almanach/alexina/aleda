#!/usr/bin/perl

binmode STDIN, ":utf8";
binmode STDOUT, ":utf8";
binmode STDERR, ":utf8";

$filters_file = shift || die "Please provide a manual filters file";
$redirect_file = shift || "";

open (FILTERS, "<$filters_file") || die "Could not open $filters_file: $!";
binmode FILTERS, ":utf8";
while (<FILTERS>) {
  chomp;
  s/^#.*//;
  if (/^-[\t;]([^\t;]+)[\t;](_\S+)$/) {
    $remove{$1."\t".$2} = 1;
  } elsif (/^-([^\t;]+)[\t;]([^\t;]+)[\t;](_\S+)$/) {
    $remove_var{$2."\t".$3}{$1} = 1;
  } elsif (/^-([^\t;]+)[\t;][\t;](_\S+)$/) {
    $multiremove_var{$1}{$2} = 1;
  } elsif (/^\+([^\t;]+)[\t;]([^\t;]+)[\t;](_\S+)$/) {
    $add_var{$2."\t".$3}{$1} = 1;
  }
}

if ($redirect_file ne "") {
  open (REDIRECT, "<$redirect_file") || die "Could not open $redirect_file: $!";
  binmode REDIRECT, ":utf8";
  while (<REDIRECT>) {
    chomp;
    s/^#.*//;
    if (/^(.*?)\t(.*)$/) {
      $var = $1;
      $nne = $2;
      if ($nne =~ s/ \((.*?)\)$//) {
	$type = $1;
	if ($type =~ /^(organisation|groupe|d[eé]partement|entreprise|revue|magazine|journal)$/i) {
	  $add_var{$2."\t_ORGANIZATION"}{$var} = 1;
	} elsif ($type =~ /^(rivière|cratère|ville|fleuve|r[eé]gion|pays|ruisseau)$/i) {
	  $add_var{$2."\t_LOCATION"}{$var} = 1;
	} elsif ($type =~ /^(acteur|patronyme|homme politique|[eé]crivain|musicien|r[eé]alisateur|footballeur|chanteur|auteur|[eé]vêque|chanteuse|athlète|artiste|compositeur|pape|historien)$/i) {
	  $add_var{$2."\t_PERSON"}{$var} = 1;
	} elsif ($type =~ /^(album|jeu vid[eé]o|bande dessin[eé]e|roman|s[eé]rie .*|comics|t[eé]l[eé]film|manga|op[eé]ra|livre)$/i) {
	  $add_var{$2."\t_WORK"}{$var} = 1;
	} else {
	  $add_var{$nne."\t_PERSON"}{$var} = 1;
	  $add_var{$nne."\t_ORGANIZATION"}{$var} = 1;
	  $add_var{$nne."\t_COMPANY"}{$var} = 1;
	  $add_var{$nne."\t_PRODUCT"}{$var} = 1;
	  $add_var{$nne."\t_WORK"}{$var} = 1;
	  $add_var{$nne."\t_LOCATION"}{$var} = 1;
	}
      } else {
	$add_var{$nne."\t_PERSON"}{$var} = 1;
	$add_var{$nne."\t_ORGANIZATION"}{$var} = 1;
	$add_var{$nne."\t_COMPANY"}{$var} = 1;
	$add_var{$nne."\t_PRODUCT"}{$var} = 1;
	$add_var{$nne."\t_WORK"}{$var} = 1;
	$add_var{$nne."\t_LOCATION"}{$var} = 1;
      }
    }
  }
}

while (<>) {
  chomp;
  /^(.*?)\t(.*?)\t(.*?)(?:_[mf])?\t(.*)$/ || die "Format error: $_";
  unless (defined($remove{"$2\t$3"})
	  || (defined($remove_var{"$2\t$3"}) && defined($remove_var{"$2\t$3"}{$1}))
	  || (defined($multiremove_var{$1}) && defined($multiremove_var{$1}{$3}))
     ) {
    print "$_\n";
  }
  if (defined($add_var{"$2\t$3"})) {
    for (keys %{$add_var{"$2\t$3"}}) {
      print "$_\t$2\t$3\t$4\n";
    }
    delete($add_var{"$2\t$3"});
  }
}
