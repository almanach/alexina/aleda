#!/usr/bin/perl

#!!! alternateNames.txt must come first, allCountries.txt afterwards in stdin

use utf8;
binmode STDIN, ":utf8";
binmode STDERR, ":utf8";
binmode STDOUT, ":utf8";

$lang = "fr";

while (1) {
  $_ = shift;
  last if (!defined($_));
  if (/^$/) {last;}
  elsif (/^-l$/) {$lang = shift;}
  else {die "Unknown option: $_";}
}

%mapping = (
#	    "A.ADM4" => { type => "_LOCATION", subtype => "CountryDivision" },
	    "A.ADMD" => { type => "_LOCATION", subtype => "CountryDivision" },
	    "A.ADM3" => { type => "_LOCATION", subtype => "CountryDivision" },
	    "A.ADM2" => { type => "_LOCATION", subtype => "CountryDivision" },
	    "A.ADM1" => { type => "_LOCATION", subtype => "CountryDivision" },
	    "A.PCLI" => { type => "_LOCATION", subtype => "Country" },
	    "A.PCLD" => { type => "_LOCATION", subtype => "CountryDivision" },
	    "A.PCLS" => { type => "_LOCATION", subtype => "Administrative" },
	    "A.TERR" => { type => "_LOCATION", subtype => "Territory" },
	    "L.RGN" => { type => "_LOCATION", subtype => "Region" },
	    "L.MILB" => { type => "_LOCATION", subtype => "POI/MilitaryBase" },
	    "L.CONT" => { type => "_LOCATION", subtype => "Region" },
	    "P.PPL" => { type => "_LOCATION", subtype => "City" },
	    "P.PPLA" => { type => "_LOCATION", subtype => "City" },
	    "P.PPLA2" => { type => "_LOCATION", subtype => "City" },
	    "P.PPLA3" => { type => "_LOCATION", subtype => "City" },
	    "P.PPLA4" => { type => "_LOCATION", subtype => "City" },
	    "P.PPLA5" => { type => "_LOCATION", subtype => "City" },
	    "P.PPLG" => { type => "_LOCATION", subtype => "City" },
	    "P.PPLC" => { type => "_LOCATION", subtype => "Capital" },
	    "P.PPLX" => { type => "_LOCATION", subtype => "POI/CitySection" },
	    "P.STLMT" => { type => "_LOCATION", subtype => "POI/IsraeliSettlement" },
	    "S.AIRP" => { type => "_LOCATION", subtype => "POI/Airport" },
	    "S.MUS" => { type => "_LOCATION", subtype => "POI/Museum" },
	    "S.STDM" => { type => "_LOCATION", subtype => "POI/Stadium" },
	    "S.MNMT" => { type => "_LOCATION", subtype => "POI/Monument" },
	    "S.UNIV" => { type => "_LOCATION", subtype => "InstitutionalOrganization" },
	    "S.CTRA" => { type => "_LOCATION", subtype => "POI/AtomicCenter" },
	    "S.CTRS" => { type => "_LOCATION", subtype => "POI/SpaceCenter" },
	   );

%lang_country = (
		 "fr" => { "FR" => 1 },
		 "en" => { "US" => 1, "GB" => 1, "IE" => 1 },
		 "es" => { "ES" => 1 },
		 "de" => { "DE" => 1 },
		);

print STDERR "  Reading files...\r";
while (<>) {
  chomp;
  $l++;
  if ($l % 1000 == 0) {print STDERR "  Reading files...$l\r"}
  s/’/'/g;
  if (/^\d+\t(\d+)\t([^\t]+)\t([^\t]+)(\t|$)/) { # alternateNames.txt
    next if ($2 eq "post"); # postal codes
    next unless ($2 eq $lang || $2 eq "");
    $id = $1; $name = $3;
    $geonames_has_name_in_current_language{$id} = 1 if ($2 eq $lang);
    next if $name !~ /^[- 0-9&.,\/a-zA-Z­°²\´\'ÂÇÈÊÉÎÏÖÜŸàáâäçèéêëíîïñóôöùûüÿ]+$/;
    $name =~ s/( [dl]') (\S)/\1\2/g;
    $l2++;
    if ($l2 % 1000 == 0) {print STDERR "\t\t\t\t$l2 A\r"}
    $geonames{$id}{$name} = $2;
  } elsif (/^(\d+)\t([^\t]+)\t[^\t]*\t([^\t]*)\t([^\t]*)\t([^\t]*)\t([^\t]*\t[^\t]*)\t([^\t]*)\t[^\t]*\t[^\t]*\t[^\t]*\t[^\t]*\t[^\t]*\t([^\t]*)\t/) {
    # P\tPPL. = capitale, A\tPCL. = pays
    $id = $1;
    $name = $2;
    $vars = $3;
    $latitude = $4;
    $longitude = $5;
    $type = $6;
    $country = $7;
    $population = $8;
    $type =~ s/\t/./;
    next if ($name !~ /[a-zA-Z\´\'ÂÇÈÊÉÎÏÖÜŸàáâäçèéêëíîïñóôöùûüÿ]/ && !defined($geonames_has_name_in_current_language{$id}));
    $name = remove_non_french_diacritics ($name);
    next if ($name !~ /[a-zA-Z\´\'ÂÇÈÊÉÎÏÖÜŸàáâäçèéêëíîïñóôöùûüÿ]/);
    next unless defined($mapping{$type});
    next if ($type =~ /^P/ && $type !~ /P\.PPLX/ && $population < 200
	     && !defined($lang_country{$lang}{$country}));
    next if ($type !~ /P\.PPLX/ && $population < 200
	     && $population > 0 
	     && !defined($lang_country{$lang}{$country}));
    $l2++;
    if ($l2 % 1000 == 0) {print STDERR "\t\t\t\t$l2     \r"}
    $geonames{$id}{$name} = 1;
    $geonames_ref{$id} = $name;
    if ($population == 0) {$span = 10}
    elsif ($type =~ /^A.PCL/) {$span = 20}
    else {$span = log (1+$population/1000)};
    if ($span < 0.1) {$span = 0.1}
    $geonames_coords{$id} = "http://maps.google.com/maps?q=$name\@$latitude,$longitude&spn=$span,$span&hl=$lang";
    $geonames_latitude{$id} = $latitude;
    $geonames_longitude{$id} = $longitude;
    $geonames_span{$id} = $span;
    $geonames_type{$id} = $type;
    $geonames_population{$id} = $population;
    $geonames_country{$id} = $country;
  } else {
    next;
  }
}
print STDERR "done\n";

print STDERR "  Printing output...";
my ($output,$ref,$coords);
for $id (keys %geonames) {
  next if (defined($geonames_is_dubious{$id}) && !defined($geonames_has_name_in_current_language{$id}));
  $ref = $geonames_ref{$id};
  $ref =~ s/^\s+//;
  $ref =~ s/\s+$//;
  $output = "geonames$id\t$ref";
  $output .= "\t";
  $coords = "";
  if (defined($geonames_coords{$id})) {
#    $coords = "<a href=\"http://maps.google.com/maps?q=$geonames_ref{$id}\@".$geonames_latitude{$id}.",".$geonames_longitude{$id}."&spn=".$geonames_span{$id}.",".$geonames_span{$id}."&hl=$lang\">Google maps</a>";
#    $coords = "<a href=\"http://www.geonames.org/$id\">Geonames</a>";
    $coords = "http://www.geonames.org/$id";
  }
  for (keys %{$geonames{$id}}) {
    next if (/^\d{4,}( |$)/);
    next if (/^\d*$/); # y compris la chaîne vide, donc
    next if $_ eq $ref;
    s/^\s+//;
    s/\s+$//;
    $output .= "$_\t";
    if (/É/) {
      s/É/E/g;
      $output .= "$_\t";
    }
  }
  print $output."$mapping{$geonames_type{$id}}{type}\t".$geonames_population{$id}."\t# \tsubtype=$mapping{$geonames_type{$id}}{subtype} country_code=$geonames_country{$id} geonames_type=$geonames_type{$id} longitude=$geonames_longitude{$id} latitude=$geonames_latitude{$id} google_maps_span=$geonames_span{$id}\t$coords\n";
}
print STDERR "done\n";



sub remove_non_french_diacritics {
  my $s = shift;
  $s =~ s/[ąãăå]/a/g;
  $s =~ s/[ćč]/c/g;
  $s =~ s/[ď]/d/g;
  $s =~ s/[ęě]/e/g;
  $s =~ s/[ğ]/g/g;
  $s =~ s/[ìíĩĭı]/i/g;
  $s =~ s/[ĺľł]/l/g;
  $s =~ s/[ńñň]/n/g;
  $s =~ s/[òóõø]/o/g;
  $s =~ s/[ŕř]/r/g;
  $s =~ s/[śšş]/s/g;
  $s =~ s/[ťţ]/t/g;
  $s =~ s/[úũǔ]/u/g;
  $s =~ s/[ỳýŷ]/y/g;
  $s =~ s/[źẑżž]/z/g;
  $s =~ s/[ĄÃĂÅ]/A/g;
  $s =~ s/[ĆČ]/C/g;
  $s =~ s/[Ď]/D/g;
  $s =~ s/[ĘĚ]/E/g;
  $s =~ s/[Ğ]/G/g;
  $s =~ s/[ÌÍĨĬİ]/I/g;
  $s =~ s/[ĹĽŁ]/L/g;
  $s =~ s/[ŃÑŇ]/N/g;
  $s =~ s/[ÒÓÕØ]/O/g;
  $s =~ s/[ŔŘ]/R/g;
  $s =~ s/[ŚŠŞ]/S/g;
  $s =~ s/[ŤŢ]/T/g;
  $s =~ s/[ÙÚŨǓ]/U/g;
  $s =~ s/[ỲÝŶ]/Y/g;
  $s =~ s/[ŹẐŻŽ]/Z/g;
  return $s;
}
