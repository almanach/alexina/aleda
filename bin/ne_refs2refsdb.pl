#!/usr/bin/perl

use utf8;
binmode STDIN, ":utf8";
binmode STDERR, ":utf8";
binmode STDOUT, ":utf8";
#use strict;
use DBI;
use Encode;

my $db = shift || "ne_refs.dat";


if (-r $db) {
  print STDERR "  Erasing previous database $db\n";
  `rm $db`;
}

my @features = qw/subtype country_code geonames_type longitude latitude google_maps_span/;

my $dbh = DBI->connect("dbi:SQLite:$db", "", "", {RaiseError => 1, AutoCommit => 0});
$dbh->do("CREATE TABLE data(key INTEGER PRIMARY KEY,name TEXT,type TEXT,weight INTEGER,def TEXT,link TEXT UNIQUE,".join(",",map {"$_ TEXT"} @features).");");
$dbh->do("CREATE TABLE mappings(lkey INTEGER, hkey INTEGER);");
my $sth=$dbh->prepare('INSERT INTO data(key,name,type,weight,def,link,'.join(",",@features).') VALUES (?,?,?,?,?,?'.(",?" x ($#features+1)).')');
my $mapping_sth=$dbh->prepare('INSERT INTO mappings(lkey,hkey) VALUES (?,?)');
my $l = 0;
print STDERR "  Loading data...";
while (<>) {
  $l++;
  if ($l % 10000 == 0) {
    print STDERR "\r  Loading data...$l";
    $dbh->commit;
  }
  chomp;
  /^(.*?)\t(.*?)\t(.*?)\t(.*?)(?:\t(.*?)(?:\t(.*?)(?:\t(.*?))?)?)?$/ || next;
  my @local_features = ($1,$2,$3,$4,$5,$7);
  my $def = $6;
  for (@features) {
    if ($def =~ /(^| )$_=(.*?)( |$)/) {
      push @local_features, $2;
    } else {
      push @local_features, "";
    }
  }
  $sth->execute(@local_features);
}
print STDERR "\r  Loading data...$l\n";
$sth->finish;
$dbh->commit;
print STDERR "  Creating index on keys...";
$dbh->do("CREATE INDEX ind ON data(key);");
print STDERR "done\n";
print STDERR "  Creating index on names...";
$dbh->do("CREATE INDEX ind_name ON data(name);");
print STDERR "done\n";
print STDERR "  Creating index on (name, type) pairs...";
$dbh->do("CREATE INDEX ind_name_type ON data(name,type);");
print STDERR "done\n";
$dbh->commit;
print STDERR "done\n";
$dbh->disconnect;
