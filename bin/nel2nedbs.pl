#!/usr/bin/perl

use utf8;
binmode STDIN, ":utf8";
binmode STDERR, ":utf8";
binmode STDOUT, ":utf8";

my %source2sourceid = (
		       "enwiki" => "0000",
		       "frwiki" => "1000",
		       "dewiki" => "1001",
		       "eswiki" => "1002",
		       "geonames" => "2000",
		       "manual" => "9999",
);

my $prenom_comp = qr/^(?:Abou Moussab|Thomas Augustine|Jean (?:François|Bernard)|Daniel Paul|Joseph Nicolas Pancrace|Nicolas Louis)/o;
my $not_a_first_name = qr/(?:(?:[lL]')?Abbé|von|van|Von|Van|de|du|of|bin|ibn|ben|Bin|Ibn)/;
my $org_prefix = qr/(?:Institut|Groupe|Consortium)/;
my $redirections_file = shift || die "Provide a redirections file";
my $homonymies_file = shift || die "Provide a homonymies file";
my $filters_file = shift || die "Provide a mft (manual filter) file";
my $nomp_lex_file = shift || die "Provide a lex file with proper nouns";
my $suff = shift || "";

my $l;
open REDIR, "<$redirections_file" || die "Could not open $redirections_file: $!";
binmode REDIR, ":utf8";
while (<REDIR>) {
  chomp;
  s/^#.*//;
  $l++;
  print STDERR "  Reading redirections...$l\r" if ($l % 100 == 0);
  if (/^(.*?)\t(.*?)(?:\t(.*)|$)/) {
    $var = $1;
    $eid = $2;
    $title = $3;
    $title =~ s/ /_/g;
    next if $var !~ /^[- 0-9&.,\/a-zA-Z­°²\´\'ÂÇÈÊÉÎÏÖÜŸàáâäçèéêëíîïñóôöùûüÿ]+$/;
    $redirs{$eid}{$var} = 1;
    $back_redirs{$var}{$eid} = 1;
    $title_redirs{$title}{$eid}{$var} = 1;
  }
}
print "\n";
close REDIR;

$l=0;
open HOMONYMY, "<$homonymies_file" || die "Could not open $homonymies_file: $!";
binmode HOMONYMY, ":utf8";
while (<HOMONYMY>) {
  chomp;
  s/^#.*//;
  $l++;
  print STDERR "  Reading homonymy links...$l\r" if ($l % 100 == 0);
  if (/^(.*?)\t(.*)$/) {
    $var = $1;
    $eid = $2;
    $redirs{$eid}{$var} = 1;
  }
}
print "\n";
close HOMONYMY;

my %seen_var;

my (%remove, %remove_var, %multiremove_var, %add_var);
open (FILTERS, "<$filters_file") || die "Could not open $filters_file: $!";
binmode FILTERS, ":utf8";
while (<FILTERS>) {
  chomp;
  s/^#.*//;
  if (/^-[\t;]([^\t;]+)[\t;](_\S+)$/) {
    $remove{$1."\t".$2} = 1; # $remove{Micheline La France\t_PERSON}
  } elsif (/^-([^\t;]+)[\t;]([^\t;]+)[\t;](_\S+)$/) {
    $remove_var{$2."\t".$3}{$1} = 1;# $remove_var{Micheline La France\t_PERSON}{France}
  } elsif (/^-([^\t;]+)[\t;][\t;](_\S+)$/) {
    $multiremove_var{$2}{$1} = 1; # $multiremove_var{France}{_PERSON}
  } elsif (/^\+([^\t;]+)[\t;]([^\t;]+)[\t;](_\S+)$/) {
    $add_var{$2."\t".$3}{$1} = 1;# $add_Var{Jacques Chirac\t_PERSON}{Chichi}
  }
}

my %first_name2gender;
if ($nomp_lex_file ne "" && $nomp_lex_file !~ /^\/fr/ && open (NOMP, "<$nomp_lex_file")) {
  while (<NOMP>) {
    next unless /\@first_name/;
    @line = split (/\t/,$_);
    if ($line[6] eq "ms") {$gender = "_m"}
    elsif ($line[6] eq "fs") {$gender = "_f"}
    else {$gender = ""}
    if (!defined($first_name2gender{$line[0]})) {
      if ($gender ne "") {
	$first_name2gender{$line[0]} = $gender;
      } else {
	$first_name2gender{$line[0]} = "";
      }
    } elsif ($first_name2gender{$line[0]} ne $gender) {
      $first_name2gender{$line[0]} = "";
    }
  }
}

open VARS, ">ne.vars$suff" || die "Could not open ne.vars: $!";
binmode VARS, ":utf8";
open REFS, ">ne.refs$suff" || die "Could not open ne.refs: $!";
binmode REFS, ":utf8";

my $nvars = 0;
my $real_ne_id;
$l=0;
my ($first_name, $first_name_re, $middle_name, $last_name, $last_name_re, $tmp_first_name);

while (<>) {
  chomp;
  $def = "";
  $l++;
  print STDERR "  Processing data...$l\r" if ($l % 100 == 0);
  if (s/\t# *(?:, *)?(.*)$//) {$def = $1; $def =~ s/\|/\//g}
  s/^([a-z]+)(\d+)\t(.*)\t(_[^\t]+)(?:\t([0-9]*))?$/\3/ || next;
  $sourceid = $1;
  $id = $2;
  $type = $4;
  $weight = $5 || "";
  $wikipedia_title_for_wikipedia_redirection_based_variants = "";
  if ($type eq "_LOCATION" && $def =~ / wikipedia_title=([^ ]+)/) {
    $wikipedia_title_for_wikipedia_redirection_based_variants = $1;
  }
  if ($weight eq "") {$weight = "NULL"}
  $real_ne_id = "000000000000".$id;
  $real_ne_id =~ s/^.*(............)$/\1/;
  die "Unknown source: $sourceid" if (!defined($source2sourceid{$sourceid}));
  $real_ne_id = $source2sourceid{$sourceid}.$real_ne_id;
  $nvars=0;
  while (s/^(.+?)\t(.+?)(\t|$)/\1\3/) {
    $redirs{$sourceid.$id}{$2} = 1;
  }
  if ($type eq "_PERSON") {
    next if /^([a-zéàè]+)\t/;
    next if /^[0-9\.,\'-]/;
    s/\t, /\t/g;
    s/ *\t */\t/g;
    s/^ +//;
    @input = split (/\t/,$_);
    $ref = $input[0];
#    print STDERR "$input[0]\n" if (/Montbrial/);
    unless (defined($remove{"$input[0]\t$type"})) {
      #    $ref_id{$input[0]}++;
      $first_name_re = "";
      $ref =~ s/^([^ ].*?[^ ]) (?:[A-Z][hr]?\.(?: (?:[A-Z][hr]?\.))*) ([^ ].*?[^ ])$/\1 \2/;
#      print STDERR "$ref\n" if (/Bush/);
      $last_name = $ref;
      $first_name = "";
      $middle_name = "";
      while ($ref =~ s/^([^ ]+) //) {
	$tmp_first_name = $1;
	last if ($tmp_first_name =~ /^$not_a_first_name$/);
	if ($first_name_re ne "") {
	  $first_name_re .= quotemeta(" ");
	}
	$first_name_re .= quotemeta($tmp_first_name);
	$last_name_re = quotemeta($ref);
#	print STDERR "? /^$first_name_re (.*) $last_name_re\$/\n" if ($ref =~ /Bush/);
	for $i (0..$#input) {
	  if ($input[$i] =~ /^$first_name_re(?: (.+))? $last_name_re$/) {
	    $last_name = $ref;
	    $middle_name = $1;
	    $middle_name_re = quotemeta($middle_name);
	    $input[0] =~ /^(.*?)(?: $middle_name)? $last_name_re$/;
	    $first_name = $1;
	  }
	}
      }
      $ref = $input[0];
#      print STDERR "> <$first_name / $middle_name / $last_name / $ref> $#input\n" if ($ref =~ /Bush/);
      if ($first_name eq "" && $ref =~ s/^($prenom_comp|[^ ]+) //) {
	$tmp = $1;
	if ($ref !~ /^(Ier|III?|I?V|VII?|VIII|I?X|XII?|XIII|XI?V|XVII?|XVIII|XI?X)( |$)/
	    && $tmp !~ /^(Ier|III?|I?V|VII?|VIII|I?X|XII?|XIII|XI?V|XVII?|XVIII|XI?X|Le|La|Styles)$/
	    && ($lang eq "fr" && ($tmp ne "Les" || $def =~ /(américain|anglais|britannique|gallois|joueur|membre fondateur|bassiste)/))
	    && ($tmp !~ /^$not_a_first_name$/)
	   ) {
	  $first_name = $tmp;
	  $first_name_re = quotemeta ($first_name);
	  $last_name = $ref;
	  $last_name_re = quotemeta ($last_name);
	}
	#	if (/^Denis Robert/) {print STDERR "!!!!! ici: $first_name / $last_name\n";}
      }
#      print STDERR "\n$ref|$first_name|$last_name|\n" if (/Bush/);
      next if $last_name =~ /^([a-zéàè \.,\'-]+)$/;
      next if $first_name =~ /^(Dictionnaire|Hommes|Association|Meilleurs|Gouverneur|Superstar|Premiers?|Compositeurs?|Famille|Nouvelle|Ministre|Meilleur|Composition|Mouvement)$/;
      %first_names = ();
#      if ($last_name !~ /^(de|du|le|la|des) / && $last_name !~ /^(les|en) /i) {
      if ($last_name !~ /^(de|du|des|le|la) / && $last_name !~ /^(les|en) /i) {
	$first_names{""} = 1;
      } elsif ($last_name =~ /^(de(?: [lL]a)?|du|des) [A-Z]/) {
	$first_names{""} = 1;
      }
      %middle_names = ();
      $middle_names{""} = 1;
      %last_names = ();
      $last_names{$last_name} = 1;
      %other_names = ();
      $gender = "";
      if ($first_name ne "") {
	$first_names{$first_name} = 1;
	if (remove_nonFrench_diacritics($first_name) ne $first_name) {
	  $first_names{remove_nonFrench_diacritics($first_name)} = 1;
	}
	if (defined ($first_name2gender{$first_name})) {
	  $gender = $first_name2gender{$first_name};
	} elsif ($first_name =~ /(tte|[ièdefgmno]ne|ia|ja|va|za|[^s]ta|[^dz]ra|la|[^ikorsu]ka|[^bhkmpuvw]ie|ee|ea|[^u]ca)$/) {
	  $gender = "_f";
	} elsif ($first_name =~ /(zo|ik|án|[^es]to|rt|[^ba]ro|rn|[^ä]rd|[^no]on|nt|[^i]nd|mo|[^e]lo|ic|[^nre]go|ek|do|co|ck|aw|av)$/) {
	  $gender = "_m";
	}
	if ($first_name =~ /^([^\'])(.*[- ])([^\'])(.*)$/) {
	  $first_names{"$1.-$3."} = 1;
	  $first_names{"$1. $3."} = 1;
	  $first_names{"$1.$3."} = 1;
	  if (remove_diacritics ($1) ne $1 || remove_diacritics ($3) ne $3) {
	    $first_names{remove_diacritics ($1)."$2".remove_diacritics ($3)."$4"} = 1;
	    $first_names{remove_diacritics ($1).".-".remove_diacritics ($3)."."} = 1;
	    $first_names{remove_diacritics ($1).". ".remove_diacritics ($3)."."} = 1;
	    $first_names{remove_diacritics ($1).".".remove_diacritics ($3)."."} = 1;
	  }
	} elsif ($first_name =~ /^([^\'])(.+)$/) {
	  $first_names{"$1."} = 1;
	  if (remove_diacritics ($1) ne $1) {
	    $first_names{remove_diacritics ($1)."."} = 1;
	    $first_names{remove_diacritics ($1).$2} = 1;
	  }
	}
      }
      if ($middle_name ne "") {
	$middle_names{$middle_name} = 1;
	if ($middle_name =~ /^([^\'])./) {
	  $middle_names{"$1."} = 1;
	}
      }
      for $i (1..$#input) {
	next if ($input[$i] =~ /^(les|en) /i);
	next if ($input[$i] =~ /^(de|du|le|la|des) /i);
	if ($input[$i] =~ /^(?:(.*?) +)?($last_name_re.*)$/) {
	  $tmp = $1;
	  $last_names{$2} = 1;
	  if ($tmp ne "" && $tmp ne "$first_name $middle_name") {
	    $first_names{$tmp} = 1;
	    if ($tmp =~ /^([^\']).*-([^\'])/) {
	      $first_names{"$1.-$2."} = 1;
	    } elsif ($tmp =~ /^([^\'])./) {
	      $first_names{"$1."} = 1;
	    }
	  }
	} elsif ($first_name ne "" && $input[$i] =~ /^($first_name_re) +(.*)$/) {
	  $last_names{$2} = 1;
	} else {
	  $other_names{$input[$i]} = 1;
	}
      }
      for $last_name (keys %last_names) {
	if (remove_nonFrench_diacritics($last_name) ne $last_name) {
	  $last_names{remove_nonFrench_diacritics($last_name)} = 1;
	}
      }
      $first_names_re = "(?:".join("|", map {quotemeta($_)} sort {length($b) <=> length($a)} keys %first_names).")";
      $first_names_re = add_robustness_to_re(remove_diacritics(remove_rare_diacritics($first_names_re)));
      $first_names_re = qr/$first_names_re/i;
      $middle_names_re = "(?:".join("|", map {quotemeta($_)} sort {length($b) <=> length($a)} keys %middle_names).")";
      $middle_names_re = add_robustness_to_re(remove_diacritics(remove_rare_diacritics($middle_names_re)));
      $middle_names_re = qr/$middle_names_re/i;
      $last_names_re = "(?:".join("|", map {quotemeta($_)} sort {length($b) <=> length($a)} keys %last_names).")";
      $last_names_re = add_robustness_to_re(remove_diacritics(remove_rare_diacritics($last_names_re)));
      $last_names_list = "\t".join("\t", keys %last_names)."\t";
      $last_names_re = qr/$last_names_re/i;
      $_ = $input[0];
      #      if (/^Denis Robert/) {print STDERR join(", ", map {"<$_>"} keys %first_names)."\n"}
      for $first_name (keys %first_names) {
	for $middle_name (keys %middle_names) {
	  next if ($first_name eq "" && $middle_name ne "");
	  next if ($first_name =~ /\.$/ && $middle_name ne "" && $middle_name !~ /\.$/);
	  for $last_name (keys %last_names) {
	    $first_name =~ s/ +/ /g; $first_name =~ s/^ //; $first_name =~ s/ $//;
	    $middle_name =~ s/ +/ /g; $middle_name =~ s/^ //; $middle_name =~ s/ $//;
	    $last_name =~ s/ +/ /g; $last_name =~ s/^ //; $last_name =~ s/ $//;
	    $output = $first_name." ".$middle_name." ".$last_name;
	    $output =~ s/ +/ /g; $output =~ s/^ //; $output =~ s/ $//;
	    $output =~ s/^'(.*)'$/\1/;
	    #	    print STDERR "unless (invalid_output($output,$input[0],$type,$real_ne_id) = ".invalid_output($output,$input[0],$type,$real_ne_id).") {\n" if (/^Denis Robert/);
	    unless (invalid_output($output,$input[0],$type,$real_ne_id)) {
	      if ($output =~ /^Abbé /) {
		print VARS "$output\t\t\t\t$output\t$real_ne_id\n";
		if (!invalid_output("l'$output",$input[0],$type,$real_ne_id)) {
		  print VARS "l'$output\t\t\t\tl'$output\t$real_ne_id\n";
		  $nvars++;
		}
	      } else {
		print VARS "$output\t$first_name\t$middle_name\t$last_name\t\t$real_ne_id\n";
	      }
	      $nvars++;
	    }
	  }
	}
      }
      for $other_name (keys %other_names) {
	$other_name =~ s/^'(.*)'$/\1/;
	unless (invalid_output($other_name,$input[0],$type,$real_ne_id)) {
	  print VARS "<$other_name>\t\t\t\t$other_name\t$real_ne_id\n";
	  if ($other_name =~ /^Abbé / && !invalid_output("l'$other_name",$input[0],$type,$real_ne_id)) {
	    print VARS "l'$other_name\t\t\t\tl'$other_name\t$real_ne_id\n";
	    $nvars++;
	  }
	  #	  print STDERR "$other_name\t\t\t\t$other_name\t$real_ne_id\n" if ($ref =~ /^Denis Robert/);
	  $nvars++;
	}
      }
      for $redir_name (keys %{$redirs{$sourceid.$id}}) {
	unless (invalid_output($redir_name,$input[0],$type,$real_ne_id)) {
	  $first_name = ""; $middle_name = ""; $last_name = "";
	  $tmp = $redir_name;
	  if ($tmp !~ /^$not_a_first_name / && $tmp =~ s/^($first_names_re) +//i) {
	    $first_name = $1;
	    if ($tmp =~ s/^($middle_names_re) +//i) {
	      $middle_name = $1;
	    }
	  }
	  $qmtmp = add_robustness_to_re(remove_diacritics(remove_rare_diacritics(quotemeta($tmp))));
	  #	  $qmtmp =~ s/\\[ \-]/[ -]*/g;
	  $qmtmp =~ qr/$qmtmp/i;
	  if ($tmp =~ /^$last_names_re( |$)/i) {
	    $last_name = $tmp;
	  } elsif ($tmp =~ /^(.*) ($last_names_re)$/i) {
	    $middle_name .= " $1";
	    $last_name = $2;
	    $middle_name =~ s/^ //;
	  } elsif ($last_names_list =~ /\t(d('|[eu] )|v[oa]n|del|de[l ]la)?$qmtmp/i) {
	    $last_name = $tmp;
	  }
	  if ($last_name ne "") {
	    print VARS "$redir_name\t$first_name\t$middle_name\t$last_name\t\t$real_ne_id\n";
	    #	    print STDERR "$redir_name\t$first_name\t$middle_name\t$last_name\t\t$real_ne_id\n" if ($ref =~ /^Denis Robert/);
	  } else {
	    print VARS "$redir_name\t\t\t\t$redir_name\t$real_ne_id\n";
	    if ($redir_name =~ /^Abbé / && !invalid_output("l'$redir_name",$input[0],$type,$real_ne_id)) {
	      print VARS "l'$redir_name\t\t\t\tl'$redir_name\t$real_ne_id\n";
	      $nvars++;
	    }
	  }
	  #	    print STDERR "$redir_name\t\t\t\t$redir_name\t$real_ne_id\n" if ($ref =~ /^Denis Robert/);
	  $nvars++;
	}
      }
      for $add_var_name (keys %{$add_var{"$_\t$type"}}) {
	unless (invalid_output($add_var_name,$input[0],$type,$real_ne_id)) {
	  print VARS "$add_var_name\t\t\t\t\t$real_ne_id\n";
#	  print STDERR "$add_var_name\t\t\t\t\t$real_ne_id\n" if ($ref =~ /^Denis Robert/);
	  $nvars++;
	}
      }
      if ($nvars > 0) {
	print REFS "$real_ne_id\t$_\t$type$gender\t$weight\t$def\n";
      }
    }
  } else {
    s/\t, /\t/g;
    s/ *\t */\t/g;
    s/^ +//;
    %input = ();
    $ref = "";
    while (s/^(.+?)(\t|$)//) {
      if ($ref eq "") {$ref = $1}
      $input{$1} = 1;
    }
    $real_ref = $ref;
    unless (defined($remove{"$real_ref\t$type"})) {
      if ($ref =~ /^(.*?)((?:, ?| )Inc\.?| Coro?poration| Ltd\.?| S\.?A\.?| A\.?G\.?| A\.?B\.?| G\.?[mM]\.?[bB]\.?H\.?| A\/S)$/ && $type eq "_COMPANY") {
	$input{$1} = 1;
      }
      $ref =~ s/\\/\\\\/g;
      %candidate_vars = ();
      for $output (keys %input, keys %{$redirs{$sourceid.$id}}, keys %{$add_var{"$ref\t$type"}}) {
	$candidate_vars{$output} = 1;
      }
      if ($type eq "_LOCATION") {
	if (defined($title_redirs{$wikipedia_title_for_wikipedia_redirection_based_variants})) {
	  for $eid (keys %{$title_redirs{$wikipedia_title_for_wikipedia_redirection_based_variants}}) {
	      for $output (keys %{$redirs{$eid}}) {
		$candidate_vars{$output} = 1 unless (length($output) < 4);
	      }
	  }
	} elsif (defined($back_redirs{$wikipedia_title_for_wikipedia_redirection_based_variants})) {
	  if (scalar keys %{$back_redirs{$wikipedia_title_for_wikipedia_redirection_based_variants}} != 1) {
	    print STDERR "WARNING: could not understand which to page leads the title '$wikipedia_title_for_wikipedia_redirection_based_variants'\n";
	  } else {
	    for my $eid (keys %{$back_redirs{$wikipedia_title_for_wikipedia_redirection_based_variants}}) {
	      for $output (keys %{$redirs{$eid}}) {
		$candidate_vars{$output} = 1 unless (length($output) < 4);
	      }
	    }
	  }
	}
      }
      for $output (keys %candidate_vars) {
	next unless $output =~ /[A-Za-z]/;
	unless (invalid_output($output,$real_ref,$type,$real_ne_id)) {
	  print VARS "$output\t\t\t\t\t$real_ne_id\n";
	  $nvars++;
	  if (($type eq "_COMPANY" || $type eq "_ORGANIZATION") && $output =~ /^($org_prefix) (.*)$/) {
	    print VARS lc($1)." $2\t\t\t\t\t$real_ne_id\n";
	    $nvars++;
	  }
	}
	$var = $output;
	if ($var =~ s/(^| )([ÁÀÂÄĄÃĂÅĆČÇĎÉÈÊËĘĚĞÌÍÎĨĬİÏĹĽŁŃÑŇÒÓÔÕÖØŔŘŚŠŞŤŢÙÚÛŨÜǓỲÝŶŸŹẐŻŽ])/$1.remove_diacritics($2)/ge) {
	  unless (invalid_output($var,$real_ref,$type,$real_ne_id)) {
	    print VARS "$var\t\t\t\t\t$real_ne_id\n";
	    $nvars++;
	  }
	}
	$var = $output;
	if ($var =~ /[̧̱̄̆̂\`\ʻ\‘\’ĀĈĊḎḐĒĖĠĢĦḤḨĪĶŌŎŜṢṬŪŬỴẔāăąǎạảấầẩậắằẳẵặĉċḍḏḑēĕėẻẽếềểễệġģĥħḥḩẖĩīĭįḯỉịķļņṉōŏơọỏốồổỗộớờợṟșṣțṭṯūŭůųưụủỨứừửữựŵŷỷỹẕ]/) {
	  $var = remove_rare_diacritics($var);
	  if ($var ne $output) {
	    unless (invalid_output($var,$real_ref,$type,$real_ne_id)) {
	      print VARS "$var\t\t\t\t\t$real_ne_id\n";
	      $nvars++;
	    }
	  }
	}
	if ($var =~ s/(^| )([ÁÀÂÄĄÃĂÅĆČÇĎÉÈÊËĘĚĞÌÍÎĨĬİÏĹĽŁŃÑŇÒÓÔÕÖØŔŘŚŠŞŤŢÙÚÛŨÜǓỲÝŶŸŹẐŻŽ])/$1.remove_diacritics($2)/ge) {
	  unless (invalid_output($var,$real_ref,$type,$real_ne_id)) {
	    print VARS "$var\t\t\t\t\t$real_ne_id\n";
	    $nvars++;
	  }
	}
      }
      if ($nvars > 0) {
	print REFS "$real_ne_id\t$ref\t$type\t$weight\t$def\n";     
      }
    }
  }
  delete ($redirs{$sourceid.$id});
  %seen_var = ();
}
print STDERR "\n";

sub invalid_output {
  my $o = shift; # variant
  my $r = shift; # reference name
  my $t = shift; # type
  my $id = shift;
  return 1 if ($seen_var{$o});
  $seen_var{$o} = 1;
  return 1 if (defined($remove_var{"$r\t$t"}{$o}));
  return 1 if (defined($multiremove_var{$t}{$o}));
  return 1 if ($o =~ /^\s*$/);
  return 1 if ($o =~ /^\(/ || $o =~ /\)$/);
  return 1 if ($o !~ /^([A-Za-zÂÇÈÉÎÏÖÜàáâäçèéêëíîïñóôöùûü0-9 \.,\'\!-]+)$/);
  return 0;
}

sub remove_diacritics {
  my $s = shift;
  $s =~ s/[áàâäąãăå]/a/g;
  $s =~ s/[ćčç]/c/g;
  $s =~ s/[ďðđ]/d/g;
  $s =~ s/[éèêëęě]/e/g;
  $s =~ s/[ğ]/g/g;
  $s =~ s/[ìíîĩĭıï]/i/g;
  $s =~ s/[ĺľł]/l/g;
  $s =~ s/[ńñň]/n/g;
  $s =~ s/[òóôõöøő]/o/g;
  $s =~ s/[ŕř]/r/g;
  $s =~ s/[śšş]/s/g;
  $s =~ s/[ťţ]/t/g;
  $s =~ s/[ùúûũüǔű]/u/g;
  $s =~ s/[ỳýŷÿ]/y/g;
  $s =~ s/[źẑżž]/z/g;
  $s =~ s/[ÁÀÂÄĄÃĂÅ]/A/g;
  $s =~ s/[ĆČÇ]/C/g;
  $s =~ s/[ĎÐ]/D/g;
  $s =~ s/[ÉÈÊËĘĚ]/E/g;
  $s =~ s/[Ğ]/G/g;
  $s =~ s/[ÌÍÎĨĬİÏ]/I/g;
  $s =~ s/[ĹĽŁ]/L/g;
  $s =~ s/[ŃÑŇ]/N/g;
  $s =~ s/[ÒÓÔÕÖØŐ]/O/g;
  $s =~ s/[ŔŘ]/R/g;
  $s =~ s/[ŚŠŞ]/S/g;
  $s =~ s/[ŤŢ]/T/g;
  $s =~ s/[ÙÚÛŨÜǓŰ]/U/g;
  $s =~ s/[ỲÝŶŸ]/Y/g;
  $s =~ s/[ŹẐŻŽ]/Z/g;
  $s =~ s/Ĳ/IJ/g;
  return $s;
}

sub remove_nonFrench_diacritics {
  my $s = shift;
  $s =~ s/[áàąãăå]/a/g;
  $s =~ s/[ćč]/c/g;
  $s =~ s/[ďðđ]/d/g;
  $s =~ s/[ęě]/e/g;
  $s =~ s/[ğ]/g/g;
  $s =~ s/[ìíĩĭı]/i/g;
  $s =~ s/[ĺľł]/l/g;
  $s =~ s/[ńñň]/n/g;
  $s =~ s/[òóõøő]/o/g;
  $s =~ s/[ŕř]/r/g;
  $s =~ s/[śšş]/s/g;
  $s =~ s/[ťţ]/t/g;
  $s =~ s/[ùúũǔű]/u/g;
  $s =~ s/[ỳýŷÿ]/y/g;
  $s =~ s/[źẑżž]/z/g;
  $s =~ s/[ÁÀÂÄĄÃĂÅ]/A/g;
  $s =~ s/[ĆČ]/C/g;
  $s =~ s/[ĎÐ]/D/g;
  $s =~ s/[ĘĚ]/E/g;
  $s =~ s/[Ğ]/G/g;
  $s =~ s/[ÌÍĨĬİ]/I/g;
  $s =~ s/[ĹĽŁ]/L/g;
  $s =~ s/[ŃÑŇ]/N/g;
  $s =~ s/[ÒÓÕØŐ]/O/g;
  $s =~ s/[ŔŘ]/R/g;
  $s =~ s/[ŚŠŞ]/S/g;
  $s =~ s/[ŤŢ]/T/g;
  $s =~ s/[ÙÚŨǓŰ]/U/g;
  $s =~ s/[ỲÝŶŸ]/Y/g;
  $s =~ s/[ŹẐŻŽ]/Z/g;
  $s =~ s/Ĳ/IJ/g;
  return $s;
}


sub remove_rare_diacritics {
  my $s = shift;
  $s =~ s/[̧̱̄̆̂]//g;
  $s =~ s/[\`\ʻ\‘\’]/\'/g;
  $s =~ s/[Ā]/A/g;
  $s =~ s/[ĈĊ]/C/g;
  $s =~ s/[ḎḐ]/D/g;
  $s =~ s/[ĒĖ]/E/g;
  $s =~ s/[ĠĢ]/G/g;
  $s =~ s/[ĦḤḨ]/H/g;
  $s =~ s/[Ī]/I/g;
  $s =~ s/[Ķ]/K/g;
  $s =~ s/[ŌŎ]/O/g;
  $s =~ s/[ŜṢ]/S/g;
  $s =~ s/[Ṭ]/T/g;
  $s =~ s/[ŪŬ]/U/g;
  $s =~ s/[Ỵ]/Y/g;
  $s =~ s/[Ẕ]/Z/g;
  $s =~ s/[āăąǎạảấầẩậắằẳẵặ]/a/g;
  $s =~ s/[ĉċ]/c/g;
  $s =~ s/[ḍḏḑ]/d/g;
  $s =~ s/[ēĕėẻẽếềểễệ]/e/g;
  $s =~ s/[ġģ]/g/g;
  $s =~ s/[ĥħḥḩẖ]/h/g;
  $s =~ s/[ĩīĭįḯỉị]/i/g;
  $s =~ s/[ķ]/k/g;
  $s =~ s/[ļ]/l/g;
  $s =~ s/[ņṉ]/n/g;
  $s =~ s/[ōŏơọỏốồổỗộớờợ]/o/g;
  $s =~ s/[ṟ]/r/g;
  $s =~ s/[șṣ]/s/g;
  $s =~ s/[țṭṯ]/t/g;
  $s =~ s/[ūŭůųưụủỨứừửữự]/u/g;
  $s =~ s/[ŵ]/w/g;
  $s =~ s/[ŷỷỹ]/y/g;
  $s =~ s/[ẕ]/z/g;
  return $s;
}

sub add_robustness_to_re {
  my $s = shift;
  $s =~ s/(^|\W)(s)(ain)(t)(\W|$)/\1\2(?:\3)?\4\5/gi;
  $s =~ s/\\[ \-]/[ -]*/g;
  $s =~ s/A/[AÁÀÂÄĄÃĂÅĀ]/g;
  $s =~ s/C/[CĆČÇĈĊ]/g;
  $s =~ s/D/[DĎÐḎḐ]/g;
  $s =~ s/E/[EÉÈÊËĘĚĒĖ]/g;
  $s =~ s/G/[GĞĠĢ]/g;
  $s =~ s/H/[HĦḤḨ]/g;
  $s =~ s/I/[IÌÍÎĨĬİÏĪ]/g;
  $s =~ s/K/[KĶ]/g;
  $s =~ s/L/[LĹĽŁ]/g;
  $s =~ s/N/[NŃÑŇ]/g;
  $s =~ s/O/[OÒÓÔÕÖØŐŌŎ]/g;
  $s =~ s/R/[RŔŘ]/g;
  $s =~ s/S/[SŚŠŞŜṢ]/g;
  $s =~ s/T/[TŤŢṬ]/g;
  $s =~ s/U/[UÙÚÛŨÜǓŰŪŬ]/g;
  $s =~ s/Y/[YỲÝŶŸ]/g;
  $s =~ s/Y/[YỴ]/g;
  $s =~ s/Z/[ZŹẐŻŽẔ]/g;
  $s =~ s/\'/[\'\`\ʻ\‘\’]/g;
  $s =~ s/a/[aáàâäąãăåāăąǎạảấầẩậắằẳẵặ]/g;
  $s =~ s/c/[cćčçĉċ]/g;
  $s =~ s/d/[dďðđḍḏḑ]/g;
  $s =~ s/e/[eéèêëęěēĕėẻẽếềểễệ]/g;
  $s =~ s/g/[gğġģ]/g;
  $s =~ s/h/[hĥħḥḩẖ]/g;
  $s =~ s/i/[iìíîĩĭıïĩīĭįḯỉị]/g;
  $s =~ s/k/[kķ]/g;
  $s =~ s/l/[lĺľłļ]/g;
  $s =~ s/n/[nńñňņṉ]/g;
  $s =~ s/o/[oòóôõöøőōŏơọỏốồổỗộớờợ]/g;
  $s =~ s/r/[rŕřṟ]/g;
  $s =~ s/s/[sśšşșṣ]/g;
  $s =~ s/t/[tťţțṭṯ]/g;
  $s =~ s/u/[uùúûũüǔűūŭůųưụủỨứừửữự]/g;
  $s =~ s/w/[wŵ]/g;
  $s =~ s/y/[yŷỷỹỳýŷÿ]/g;
  $s =~ s/z/[zźẑżžẕ]/g;
  $s =~ s/(Ĳ|IJ)/IJ/g;
  return $s;
}
