#!/usr/bin/perl

use utf8;
binmode STDIN, ":utf8";
binmode STDERR, ":utf8";
binmode STDOUT, ":utf8";
#use strict;
use DBI;
use Encode;

my $db = shift || "ne_vars.dat";


if (-r $db) {
  print STDERR "  Erasing previous database $db\n";
  `rm $db`;
}

my $dbh = DBI->connect("dbi:SQLite:$db", "", "", {RaiseError => 1, AutoCommit => 0});
$dbh->do("CREATE TABLE data(key INTEGER,variant TEXT, firstname TEXT, middlename TEXT, lastname TEXT, othername TEXT);");
my $sth=$dbh->prepare('INSERT INTO data(key,variant,firstname,middlename,lastname,othername) VALUES (?,?,?,?,?,?)');
my $l = 0;
print STDERR "  Loading data...";
while (<>) {
  $l++;
  if ($l % 10000 == 0) {
    print STDERR "\r  Loading data...$l";
    $dbh->commit;
  }
  chomp;
  /^(.*?)\t(.*)\t(.*)\t(.*)\t(.*)\t(.*)$/ || next;
  $sth->execute($6,$1,$2,$3,$4,$5)
}
print STDERR "\r  Loading data...$l\n";
$sth->finish;
$dbh->commit;
print STDERR "  Creating index on ids...";
$dbh->do("CREATE INDEX ind ON data(key);");
$dbh->commit;
print STDERR "done\n";
print STDERR "  Creating index on strings...";
$dbh->do("CREATE INDEX vind ON data(variant);");
$dbh->commit;
print STDERR "done\n";
$dbh->disconnect;
