#!/usr/bin/perl

use utf8;
binmode STDIN, ":utf8";
binmode STDOUT, ":utf8";
binmode STDERR, ":utf8";

$infoboxes = shift || die;

print STDERR "Loading infoboxes\n";
open IB, "<$infoboxes" || die $!;
binmode IB, ":utf8";
while (<IB>) {
  chomp;
  /^(.*?)\t(_.*)$/ || next;
  $tmpl = $1;
  $type = $2;
  $tmpl =~ s|^http://dbpedia.org/resource/Modèle|http://dbpedia.org/resource/Template|;
  $tmpl =~ s|^http://dbpedia.org/resource/Template:||;
  $tmpl =~ s|_| |g;
  $tmpl =~ s|^Infobox ||g;
  $infobox2type{$tmpl} = $type;
}
close IB;

while (<>) {
  chomp;
  $l++;
  if ($l % 100000 == 0) {print STDERR "Reading wikipedia XML file $l ($npages pages)\r"}
  if (/^\s*<article /) {
    if (/wikipedia_title=\"(.*?)\"/) {
      $wikipedia_title = $1;
      $npages++;
    }
  } elsif (/^\s*<infobox /) {
    if (/title=\"(.*?)\"/) {
      if (defined ($infobox2type{$1})) {
	$candidate{$wikipedia_title}{$infobox2type{$1}}++;
      }
    }
  }
}
print STDERR "\n";

for $t (keys %candidate) {
  if (scalar %{$candidate{$t}} == 1) {
    for (keys %{$candidate{$t}}) {
      $line = $t."\t".$_."\n";
      $line =~ s/_/ /g;
      print $line;
    }
  }
}
