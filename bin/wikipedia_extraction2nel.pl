#!/usr/bin/perl

use utf8;
binmode STDIN, ":utf8";
binmode STDOUT, ":utf8";
binmode STDERR, ":utf8";

$cat_type_file = shift || die;
open CATTYPE, "<$cat_type_file" || die $!;
binmode CATTYPE, ":utf8";
while (<CATTYPE>) {
  chomp;
  /^(.+?)\s*\t\s*(.+?)(\t|$)/ || next;
  $cat = $1;
  $type = $2;
  if ($type !~ /^_/) {$type = "_$type"}
  $cat2type{$cat} = $type;
}

$infobox_data_file = shift || die;
open IBDATA, "<$infobox_data_file" || die $!;
binmode IBDATA, ":utf8";
while (<IBDATA>) {
  chomp;
  /^(.+?)\s*\t\s*(.+?)(\t|$)/ || next;
  $title = $1;
  $type = $2;
  if ($type !~ /^_/) {$type = "_$type"}
  $ib_title2type{$title} = $type;
}

my $lang;

while (<>) {
  chomp;
  /^(...?)(wiki\d+)\t(.*?)\t(.*?)\t(.*?)\t(.*?)\t(.*?)$/ || next;
  $lang = $1;
  $id = $1.$2;
  $wtitle = $3;
  $title = $4;
  $weight = $5;
  $cats = $6;
  $remainder = $7;
  %types = ();
  for (split (/\|/, $cats)) {
    $type = generic_category2type($_);
    if ($type =~ /^(.*):(\d+)/) {
      $types{$1}+=$2;
    } elsif ($type ne "") {
      $types{$type}+=1;
    }
    if (defined($cat2type{$_})) {
      $types{$cat2type{$_}}+=0.5;
    }
  }
  if (defined($ib_title2type{$title})) {
    next if ($ib_title2type{$title} eq "_LOCATION");
    $canonical_name = $title;
    $canonical_name =~ s/^(.*) \(.*\)$/\1/;
    $id =~ /^(.*?)wiki/ || die;
    print "$id\t$canonical_name\t$ib_title2type{$title}\t$weight\t# $remainder\t\thttp://$1.wikipedia.org/wiki/$wtitle\n";
  } elsif (scalar keys %types > 0) {
    next if ($wtitle =~ /^Liste (des? |du |d')[^ ].{3}/);
    $type = (sort {$types{$b} <=> $types{$a}} keys %types)[0];
    next if ($type eq "_LOCATION");
    $canonical_name = $title;
    $canonical_name =~ s/^(.*) \(.*\)$/\1/;
    $id =~ /^(.*?)wiki/ || die;
    print "$id\t$canonical_name\t$type\t$weight\t# $remainder\t\thttp://$1.wikipedia.org/wiki/$wtitle\n";
    if (defined($ib_title2type{$title}) && $ib_title2type{$title} ne $type) {
      print STDERR "### WARNING: $title is considered as a $ib_title2type{$title} but could also be a $type\n";
    }
  }
}

sub generic_category2type {
  $c = shift;
  if ($lang eq "fr") {
    if ($c =~ /^(Animal célèbre)( |$)/i) {
      return "_ANIMAL:6";
    } elsif ($c =~ /^(Naissance|Décès|Personnalité|Scientifique|Ingénieur|Membre|Chanteur|Joueur|Chevalier|Consultant|Polytechnicien|Écrivain|Réalisateur|Député|Poète|Dramaturge|Compositeur|Designer|Linguiste|Normalien|Inventeur|Chimiste|Archéologue|Philosophe|Lauréat|Ministre|Pape|Président|Peintre|Mathématicien|Scénariste|Physicien|Monarque|Actrice|Femme|Homme)( |$)/i) {
      return "_PERSON";
    } elsif ($c =~ /^(Commune d(u|es?)|Cours d\'eau|Comté de|Ville|Lac de|Village et ville d(?:u|es?)|Subdivision admnistrative|Siège de compté d(?:u|es?)|Arrondissement d(?:u|es?)|Station thermale|Canton d(?:u|es?)|Oblast d(?:u|es?)|Commune membre|Village|Localité d(?:u|es?)|Ville portuaire|Municipalité d(?:u|es?)|Pays|Port d(?:u|es?))( |$)/i) {
      return "_LOCATION";
    } elsif ($c =~ /^(Entreprise|Filiale|Groupe de médias|Société ayant son siège|Société cotée|Société d'études?|Presse en ligne|Institut de sondage)( |$)/i) {
      return "_COMPANY";
    } elsif ($c =~ /^(Organisations?|Organismes?|Partis?|Académies?|Universités?|Titres? de presse|Associations?|Radio|Organe législatif|Agence de presse|Institutions?|Juridiction administrative|Juridiction d'appel|Juridiction de l'Ancien Régime|Juridiction judiciaire française|Administration|Club|Festival|Think tank)( |$)/i && $c !~ /^(Organisme modèle)( |$)/i) {
      return "_ORGANIZATION";
    } elsif ($c =~ /^(Logiciel|Marque)( |$)/i) { # Système
      return "_PRODUCT";
    } elsif ($c =~ /^(Film|Roman|Objet conservé (à|au))( |$)/i) { # Série
      return "_WORK:8";
    }
  } elsif ($lang eq "de") {
    if ($c =~ /^(Geboren|Gestorben|Mann|Frau)( |$)/i) {
      return "_PERSON";
    }
  }
  return "";
}

