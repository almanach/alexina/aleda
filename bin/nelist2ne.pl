#!/usr/bin/perl

use utf8;
binmode STDIN, ":utf8";
binmode STDERR, ":utf8";
binmode STDOUT, ":utf8";

$prenom_comp = qr/^(?:Abou Moussab|Thomas Augustine|Jean (?:François|Bernard)|Daniel Paul|Joseph Nicolas Pancrace|Nicolas Louis)/o;
my $nomp_lex_file = shift || "";

my %first_name2gender;
if ($nomp_lex_file ne "" && $nomp_lex_file !~ /^\/fr/ && open (NOMP, "<$nomp_lex_file")) {
  while (<NOMP>) {
    next unless /\@first_name/;
    @line = split (/\t/,$_);
    if ($line[6] eq "ms") {$gender = "_m"}
    elsif ($line[6] eq "fs") {$gender = "_f"}
    else {$gender = ""}
    if (!defined($first_name2gender{$line[0]})) {
      if ($gender ne "") {
	$first_name2gender{$line[0]} = $gender;
      } else {
	$first_name2gender{$line[0]} = "";
      }
    } elsif ($first_name2gender{$line[0]} ne $gender) {
      $first_name2gender{$line[0]} = "";
    }
  }
}


while (<>) {
  chomp;
  $def = "";
  if (s/\t# *(?:, *)?(.*)$//) {$def = $1; $def =~ s/\|/\//g}
  s/\t(_[^\t]+)(?:\t([0-9]*))?$// || next;
  $type = $1;
  $weight = $2 || 10;
  if ($weight eq "") {$weight = "0"}
  if ($type eq "_PERSON") {
    next if /^([a-zéàè]+)\t/;
    next if /^[0-9\.,\'-]/;
    s/\t, /\t/g;
    s/ *\t */\t/g;
    s/^ +//;
    @input = split (/\t/,$_);
    $ref = $input[0];
    $ref_id{$input[0]}++;
    $first_name_re = "";
    $last_name = $ref;
    $first_name = "";
    $middle_name = "";
    while ($ref =~ s/^([^ ]+) //) {
      if ($first_name_re ne "") {
	$first_name_re .= quotemeta(" ");
      }
      $first_name_re .= quotemeta($1);
      $last_name_re = quotemeta($ref);
      for $i (1..$#input) {
	if ($input[$i] =~ /^$first_name_re (.*) $last_name_re$/) {
	  $last_name = $ref;
	  $middle_name = $1;
	  $input[0] =~ /^(.*) $last_name_re$/;
	  $first_name = $1;
	}
      }
    }
    $ref = $input[0];
    if ($first_name eq "" && $ref =~ s/^($prenom_comp|[^ ]+) //) {
      $tmp = $1;
      if ($ref !~ /^(Ier|III?|I?V|VII?|VIII|I?X|XII?|XIII|XI?V|XVII?|XVIII|XI?X)( |$)/
	 && $tmp !~ /^(Ier|III?|I?V|VII?|VIII|I?X|XII?|XIII|XI?V|XVII?|XVIII|XI?X|Le|La|Styles)$/
	 && ($tmp ne "Les" || $def =~ /(américain|anglais|britannique|gallois|joueur|membre fondateur|bassiste)/)) {
	$first_name = $tmp;
	$first_name_re = quotemeta ($first_name);
	$last_name = $ref;
	$last_name_re = quotemeta ($last_name);
      }
    }
    next if $last_name =~ /^([a-zéàè \.,\'-]+)$/;
    next if $first_name =~ /^(Dictionnaire|Hommes|Association|Meilleurs|Gouverneur|Superstar|Premiers?|Compositeurs?|Famille|Nouvelle|Ministre|Meilleur|Composition|Mouvement)$/;
    %first_names = ();
    if ($last_name !~ /^(de|du|le|la|des) / && $last_name !~ /^(les|en) /i) {
      $first_names{""} = 1;
    } elsif ($last_name =~ /^(de Sarnez)$/) {
      $first_names{""} = 1;
    }
    %middle_names = ();
    $middle_names{""} = 1;
    %last_names = ();
    $last_names{$last_name} = 1;
    %other_names = ();
    $gender = "";
    if ($first_name ne "") {
      $first_names{$first_name} = 1;
      if (defined ($first_name2gender{$first_name})) {
	$gender = $first_name2gender{$first_name};
      } elsif ($first_name =~ /(tte|[ièdefgmno]ne|ia|ja|va|za|[^s]ta|[^dz]ra|la|[^ikorsu]ka|[^bhkmpuvw]ie|ee|ea|[^u]ca)$/) {
	$gender = "_f";
      } elsif ($first_name =~ /(zo|ik|án|[^es]to|rt|[^ba]ro|rn|[^ä]rd|[^no]on|nt|[^i]nd|mo|[^e]lo|ic|[^nre]go|ek|do|co|ck|aw|av)$/) {
	$gender = "_m";
      }
      if ($first_name =~ /^([^\'])(.*[- ])([^\'])(.*)$/) {
	$first_names{"$1.-$3."} = 1;
	$first_names{"$1. $3."} = 1;
	$first_names{"$1.$3."} = 1;
	if (remove_diacritics ($1) ne $1 || remove_diacritics ($3) ne $3) {
	  $first_names{remove_diacritics ($1)."$2".remove_diacritics ($3)."$4"} = 1;
	  $first_names{remove_diacritics ($1).".-".remove_diacritics ($3)."."} = 1;
	  $first_names{remove_diacritics ($1).". ".remove_diacritics ($3)."."} = 1;
	  $first_names{remove_diacritics ($1).".".remove_diacritics ($3)."."} = 1;
	}
      } elsif ($first_name =~ /^([^\'])(.+)$/) {
	$first_names{"$1."} = 1;
	if (remove_diacritics ($1) ne $1) {
	  $first_names{remove_diacritics ($1)."."} = 1;
	  $first_names{remove_diacritics ($1).$2} = 1;
	}
      }
    }
    if ($middle_name ne "") {
      $middle_names{$middle_name} = 1;
      if ($middle_name =~ /^([^\'])./) {
	$middle_names{"$1."} = 1;
      }
    }
    for $i (1..$#input) {
      next if ($input[$i] =~ /^(les|en) /i);
      next if ($input[$i] =~ /^(de|du|le|la|des) /i);
      if ($input[$i] =~ /^(?:(.*?) +)?($last_name_re.*)$/) {
	$tmp = $1;
	$last_names{$2} = 1;
	if ($tmp ne "" && $tmp ne "$first_name $middle_name") {
	  $first_names{$tmp} = 1;
	  if ($tmp =~ /^([^\']).*-([^\'])/) {
	    $first_names{"$1.-$2."} = 1;
	  } elsif ($tmp =~ /^([^\'])./) {
	    $first_names{"$1."} = 1;
	  }
	}
      } elsif ($first_name ne "" && $input[$i] =~ /^($first_name_re) +(.*)$/) {
	$last_names{$2} = 1;
      } else {
	$other_names{$input[$i]} = 1;
      }
    }
    $_ = $input[0];
    if ($ref_id{$_} > 1) {
      $_ .= " ($ref_id{$_})";
    }
    for $first_name (keys %first_names) {
      for $middle_name (keys %middle_names) {
	next if ($first_name eq "" && $middle_name ne "");
	next if ($first_name =~ /\.$/ && $middle_name ne "" && $middle_name !~ /\.$/);
	for $last_name (keys %last_names) {
	  $output = $first_name." ".$middle_name." ".$last_name;
	  $output =~ s/ +/ /g; $output =~ s/^ //;
	  $output =~ s/^'(.*)'$/\1/;
	  print "$output\t$_\t$type$gender\t# $weight $def\n" unless invalid_output($output);
	}
      }
    }
    for $other_name (keys %other_names) {
      $other_name =~ s/^'(.*)'$/\1/;
      print "$other_name\t$_\t$type\t# $weight $def\n" unless invalid_output($output);
    }
  } else {
    s/\t, /\t/g;
    s/ *\t */\t/g;
    s/^ +//;
    %input = ();
    $ref = "";
    while (s/^(.+?)(\t|$)//) {
      if ($ref eq "") {$ref = $1}
      $input{$1} = 1;
    }
    $ref_id{$ref}++;
    if ($ref_id{$ref} > 1) {
      $ref .= " ($ref_id{$ref})";
    }
    if ($ref =~ /^(.*?)((?:, ?| )Inc\.?| Coroporation| Ltd\.?| S\.?A\.?| A\.?G\.?| A\.?B\.?| G\.?[mM]\.?B\.?H\.?| A\/S)$/ && $type eq "_COMPANY") {
      $input{$1} = 1;
    }
    $ref =~ s/\\/\\\\/g;
    for $output (keys %input) {
      next unless $output =~ /[A-Za-z]/;
      print "$output\t$ref\t$type\t# $weight $def\n" unless invalid_output($output);
      if ($output =~ s/(^| )([ÂÇÈÉÎÏÖÜ])/$1.remove_diacritics($2)/ge) {
	print "$output\t$ref\t$type\t# $weight $def\n" unless invalid_output($output);
      }
    }
  }
}

sub invalid_output {
  my $o = shift;
  return 1 if ($o =~ /^\s*$/);
  return 1 if ($o =~ /^\(/ || $o =~ /\)$/);
  return 1 if ($o !~ /^([A-Za-zÂÇÈÉÎÏÖÜàáâäçèéêëíîïñóôöùûü0-9 \.,\'\!-]+)$/);
  return 0;
}

sub remove_diacritics {
  my $s = shift;
  $s =~ s/[áàâäąãăå]/a/g;
  $s =~ s/[ćčç]/c/g;
  $s =~ s/[ď]/d/g;
  $s =~ s/[éèêëęě]/e/g;
  $s =~ s/[ğ]/g/g;
  $s =~ s/[ìíîĩĭıï]/i/g;
  $s =~ s/[ĺľł]/l/g;
  $s =~ s/[ńñň]/n/g;
  $s =~ s/[òóôõöø]/o/g;
  $s =~ s/[ŕř]/r/g;
  $s =~ s/[śšş]/s/g;
  $s =~ s/[ťţ]/t/g;
  $s =~ s/[ùúûũüǔ]/u/g;
  $s =~ s/[ỳýŷÿ]/y/g;
  $s =~ s/[źẑżž]/z/g;
  $s =~ s/[ÁÀÂÄĄÃĂÅ]/A/g;
  $s =~ s/[ĆČÇ]/C/g;
  $s =~ s/[Ď]/D/g;
  $s =~ s/[ÉÈÊËĘĚ]/E/g;
  $s =~ s/[Ğ]/G/g;
  $s =~ s/[ÌÍÎĨĬİÏ]/I/g;
  $s =~ s/[ĹĽŁ]/L/g;
  $s =~ s/[ŃÑŇ]/N/g;
  $s =~ s/[ÒÓÔÕÖØ]/O/g;
  $s =~ s/[ŔŘ]/R/g;
  $s =~ s/[ŚŠŞ]/S/g;
  $s =~ s/[ŤŢ]/T/g;
  $s =~ s/[ÙÚÛŨÜǓ]/U/g;
  $s =~ s/[ỲÝŶŸ]/Y/g;
  $s =~ s/[ŹẐŻŽ]/Z/g;
  return $s;
}
