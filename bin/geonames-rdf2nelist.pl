#!/usr/bin/perl

use utf8;
binmode STDIN, ":utf8";
binmode STDERR, ":utf8";
binmode STDOUT, ":utf8";
use URI::Escape;
use Encode;

$lang = "fr";

while (1) {
  $_ = shift;
  last if (!defined($_));
  if (/^$/) {last;}
  elsif (/^-l$/) {$lang = shift;}
  else {die "Unknown option: $_";}
}

%mapping = (
#	    "A.ADM4" => { type => "_LOCATION", subtype => "CountryDivision" },
	    "A.ADMD" => { type => "_LOCATION", subtype => "CountryDivision" },
	    "A.ADM3" => { type => "_LOCATION", subtype => "CountryDivision" },
	    "A.ADM2" => { type => "_LOCATION", subtype => "CountryDivision" },
	    "A.ADM1" => { type => "_LOCATION", subtype => "CountryDivision" },
	    "A.PCLI" => { type => "_LOCATION", subtype => "Country" },
	    "A.PCLD" => { type => "_LOCATION", subtype => "CountryDivision" },
	    "A.PCLS" => { type => "_LOCATION", subtype => "Administrative" },
	    "A.TERR" => { type => "_LOCATION", subtype => "Territory" },
	    "L.RGN" => { type => "_LOCATION", subtype => "Region" },
	    "L.MILB" => { type => "_LOCATION", subtype => "POI/MilitaryBase" },
	    "L.CONT" => { type => "_LOCATION", subtype => "Region" },
	    "P.PPL" => { type => "_LOCATION", subtype => "City" },
	    "P.PPLA" => { type => "_LOCATION", subtype => "City" },
	    "P.PPLA2" => { type => "_LOCATION", subtype => "City" },
	    "P.PPLA3" => { type => "_LOCATION", subtype => "City" },
	    "P.PPLA4" => { type => "_LOCATION", subtype => "City" },
	    "P.PPLA5" => { type => "_LOCATION", subtype => "City" },
	    "P.PPLG" => { type => "_LOCATION", subtype => "City" },
	    "P.PPLC" => { type => "_LOCATION", subtype => "Capital" },
	    "P.PPLX" => { type => "_LOCATION", subtype => "POI/CitySection" },
	    "P.STLMT" => { type => "_LOCATION", subtype => "POI/IsraeliSettlement" },
	    "S.AIRP" => { type => "_LOCATION", subtype => "POI/Airport" },
	    "S.MUS" => { type => "_LOCATION", subtype => "POI/Museum" },
	    "S.STDM" => { type => "_LOCATION", subtype => "POI/Stadium" },
	    "S.MNMT" => { type => "_LOCATION", subtype => "POI/Monument" },
	    "S.UNIV" => { type => "_LOCATION", subtype => "InstitutionalOrganization" },
	    "S.CTRA" => { type => "_LOCATION", subtype => "POI/AtomicCenter" },
	    "S.CTRS" => { type => "_LOCATION", subtype => "POI/SpaceCenter" },
	   );

%lang_country = (
		 "fr" => { "FR" => 1 },
		 "en" => { "US" => 1, "GB" => 1, "IE" => 1 },
		 "es" => { "ES" => 1 },
		 "de" => { "DE" => 1 },
		);

print STDERR "  Reading files...\r";
while (<>) {
  chomp;
  $l++;
  if ($l % 1000 == 0) {print STDERR "  Reading files...$l\r"}
  s/’/'/g;
#<?xml version="1.0" encoding="UTF-8" standalone="no"?><rdf:RDF xmlns:cc="http://creativecommons.org/ns#" xmlns:dcterms="http://purl.org/dc/terms/" xmlns:foaf="http://xmlns.com/foaf/0.1/" xmlns:gn="http://www.geonames.org/ontology#" xmlns:owl="http://www.w3.org/2002/07/owl#" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#" xmlns:rdfs="http://www.w3.org/2000/01/rdf-schema#" xmlns:wgs84_pos="http://www.w3.org/2003/01/geo/wgs84_pos#"><gn:Feature rdf:about="http://sws.geonames.org/75337/"><rdfs:isDefinedBy>http://sws.geonames.org/75337/about.rdf</rdfs:isDefinedBy><gn:name>Ḩajjah</gn:name><gn:alternateName xml:lang="ar">حجة</gn:alternateName><gn:alternateName xml:lang="ru">Хадж</gn:alternateName><gn:alternateName xml:lang="ar">Ḩajjah</gn:alternateName><gn:alternateName>Hajja</gn:alternateName><gn:alternateName>Ḥaggah</gn:alternateName><gn:alternateName>Ḥaggiah</gn:alternateName><gn:alternateName>Hajje</gn:alternateName><gn:alternateName>Ḥage</gn:alternateName><gn:featureClass rdf:resource="http://www.geonames.org/ontology#P"/><gn:featureCode rdf:resource="http://www.geonames.org/ontology#P.PPLA"/><gn:countryCode>YE</gn:countryCode><gn:population>43549</gn:population><wgs84_pos:lat>15.69425</wgs84_pos:lat><wgs84_pos:long>43.60582</wgs84_pos:long><gn:parentFeature rdf:resource="http://sws.geonames.org/75334/"/><gn:parentCountry rdf:resource="http://sws.geonames.org/69543/"/><gn:parentADM1 rdf:resource="http://sws.geonames.org/6201195/"/><gn:parentADM2 rdf:resource="http://sws.geonames.org/75334/"/><gn:nearbyFeatures rdf:resource="http://sws.geonames.org/75337/nearby.rdf"/><gn:locationMap rdf:resource="http://www.geonames.org/75337/hajjah.html"/><gn:wikipediaArticle rdf:resource="http://it.wikipedia.org/wiki/Hajjah"/><gn:wikipediaArticle rdf:resource="http://pl.wikipedia.org/wiki/Had%C5%BCd%C5%BCa"/><gn:wikipediaArticle rdf:resource="http://en.wikipedia.org/wiki/Hajjah"/><rdfs:seeAlso rdf:resource="http://dbpedia.org/resource/Hajjah"/></gn:Feature></rdf:RDF>
  next unless /^<\?xml/;
  die $_ unless (/<gn:Feature rdf:about="http:\/\/sws.geonames.org\/(\d+)\/">/);
  $id = $1;
  die unless (/<gn:name>(.*)<\/gn:name>/);
  $name = $1;
  if (/<wgs84_pos:lat>(.*?)<\/wgs84_pos:lat>/) {$latitude = $1;} else {$latitude = "";}
  if (/<wgs84_pos:long>(.*?)<\/wgs84_pos:long>/) {$longitude = $1;} else {$longitude = "";}
  if (/<wgs84_pos:alt>(.*?)<\/wgs84_pos:alt>/) {$altitude = $1;} else {$altitude = "";}
  if (/<gn:featureCode rdf:resource="http:\/\/www.geonames.org\/ontology#(.*?)"\/>/) {
    $type = $1;
  } elsif (/<gn:featureClass rdf:resource="http:\/\/www.geonames.org\/ontology#(.*?)"\/>/) {
    $type = $1;
  } else {
    print STDERR "### WARNING: no type in the following line:\n$_\n";
    next;
  }
  if (/<gn:countryCode>(.*?)<\/gn:countryCode>/) {$country = $1;} else {$country = "";}
  if (/<gn:population>(.*?)<\/gn:population>/) {$population = $1;} else {$population = "";}
  if (/<gn:wikipediaArticle rdf:resource="http:\/\/$lang.wikipedia.org\/wiki\/(.*?)"\/>/) {$wikipedia_page = Encode::decode('utf8', uri_unescape($1));} else {$wikipedia_page = "";}
  $wikipedia_page =~ s/ /_/g;
  $type =~ s/\t/./;
  next if ($name !~ /[a-zA-Z\´\'ÂÇÈÊÉÎÏÖÜŸàáâäçèéêëíîïñóôöùûüÿ]/ && !defined($geonames_has_name_in_current_language{$id}));
  $name = remove_non_french_diacritics ($name);
  next if ($name !~ /[a-zA-Z\´\'ÂÇÈÊÉÎÏÖÜŸàáâäçèéêëíîïñóôöùûüÿ]/);
  next unless defined($mapping{$type});
  next if ($type =~ /^P/ && $type !~ /P\.PPLX/ && $population < 200
	   && !defined($lang_country{$lang}{$country}));
  next if ($type !~ /P\.PPLX/ && $population < 200
	   && $population > 0 
	     && !defined($lang_country{$lang}{$country}));
  while (/<gn:alternateName(?: xml:lang="([^"]*)")?>(.*?)<\/gn:alternateName>/g) {
    $local_lang = $1;
    next unless ($local_lang eq $lang || $local_lang eq "");
    $altname = $2;
    $geonames_has_name_in_current_language{$id} = 1 if ($local_lang eq $lang);
    next if $altname !~ /^[- 0-9&.,\/a-zA-Z­°²\´\'ÂÇÈÊÉÎÏÖÜŸàáâäçèéêëíîïñóôöùûüÿ]+$/;
    $altname =~ s/( [dl]') (\S)/\1\2/g;
    $geonames{$id}{$altname} = $local_lang;
  }
  $geonames{$id}{$name} = 1;
  $geonames_ref{$id} = $name;
  if ($population == 0) {$span = 10}
  elsif ($type =~ /^A.PCL/) {$span = 20}
  else {$span = log (1+$population/1000)};
  if ($span < 0.1) {$span = 0.1}
  $geonames_coords{$id} = "http://maps.google.com/maps?q=$name\@$latitude,$longitude&spn=$span,$span&hl=$lang";
  $geonames_latitude{$id} = $latitude;
  $geonames_longitude{$id} = $longitude;
  $geonames_altitude{$id} = $altitude;
  $geonames_wikipedia_page{$id} = $wikipedia_page;
  $geonames_span{$id} = $span;
  $geonames_type{$id} = $type;
  $geonames_population{$id} = $population;
  $geonames_country{$id} = $country;
}
print STDERR "done\n";

print STDERR "  Printing output...";
my ($output,$ref,$coords);
for $id (keys %geonames) {
#  next if (defined($geonames_is_dubious{$id}) && !defined($geonames_has_name_in_current_language{$id}));
  $ref = $geonames_ref{$id};
  $ref =~ s/^\s+//;
  $ref =~ s/\s+$//;
  $output = "geonames$id\t$ref";
  $output .= "\t";
  $coords = "";
  if (defined($geonames_coords{$id})) {
#    $coords = "<a href=\"http://maps.google.com/maps?q=$geonames_ref{$id}\@".$geonames_latitude{$id}.",".$geonames_longitude{$id}."&spn=".$geonames_span{$id}.",".$geonames_span{$id}."&hl=$lang\">Google maps</a>";
#    $coords = "<a href=\"http://www.geonames.org/$id\">Geonames</a>";
    $coords = "http://www.geonames.org/$id";
  }
  for (keys %{$geonames{$id}}) {
    next if (/^\d{4,}( |$)/);
    next if (/^\d*$/); # y compris la chaîne vide, donc
    next if $_ eq $ref;
    s/^\s+//;
    s/\s+$//;
    $output .= "$_\t";
    if (/É/) {
      s/É/E/g;
      $output .= "$_\t";
    }
  }
  print $output."$mapping{$geonames_type{$id}}{type}\t".$geonames_population{$id}."\t# \tsubtype=$mapping{$geonames_type{$id}}{subtype} country_code=$geonames_country{$id} geonames_type=$geonames_type{$id} longitude=$geonames_longitude{$id} latitude=$geonames_latitude{$id} altitude=$geonames_altitude{$id} ";
  print "wikipedia_title=$geonames_wikipedia_page{$id} " unless ($geonames_wikipedia_page{$id} eq "");
  print "google_maps_span=$geonames_span{$id}\t$coords\n";
}
print STDERR "done\n";



sub remove_non_french_diacritics {
  my $s = shift;
  $s =~ s/[ąãăå]/a/g;
  $s =~ s/[ćč]/c/g;
  $s =~ s/[ď]/d/g;
  $s =~ s/[ęě]/e/g;
  $s =~ s/[ğ]/g/g;
  $s =~ s/[ìíĩĭı]/i/g;
  $s =~ s/[ĺľł]/l/g;
  $s =~ s/[ńñň]/n/g;
  $s =~ s/[òóõø]/o/g;
  $s =~ s/[ŕř]/r/g;
  $s =~ s/[śšş]/s/g;
  $s =~ s/[ťţ]/t/g;
  $s =~ s/[úũǔ]/u/g;
  $s =~ s/[ỳýŷ]/y/g;
  $s =~ s/[źẑżž]/z/g;
  $s =~ s/[ĄÃĂÅ]/A/g;
  $s =~ s/[ĆČ]/C/g;
  $s =~ s/[Ď]/D/g;
  $s =~ s/[ĘĚ]/E/g;
  $s =~ s/[Ğ]/G/g;
  $s =~ s/[ÌÍĨĬİ]/I/g;
  $s =~ s/[ĹĽŁ]/L/g;
  $s =~ s/[ŃÑŇ]/N/g;
  $s =~ s/[ÒÓÕØ]/O/g;
  $s =~ s/[ŔŘ]/R/g;
  $s =~ s/[ŚŠŞ]/S/g;
  $s =~ s/[ŤŢ]/T/g;
  $s =~ s/[ÙÚŨǓ]/U/g;
  $s =~ s/[ỲÝŶ]/Y/g;
  $s =~ s/[ŹẐŻŽ]/Z/g;
  return $s;
}
