#!/usr/bin/perl

use locale;
use utf8;
#use URI::Escape; #inlined
use Encode;
binmode STDIN, ":encoding(iso-8859-1)";
binmode STDOUT, ":utf8";
binmode STDERR, ":utf8";

$infoboxes = shift || die;
$links = shift || die;

$|=1;

print STDERR "Loading infoboxes\n";
open IB, "<$infoboxes" || die $!;
binmode IB, ":utf8";
while (<IB>) {
  chomp;
  /^(.*?)\t(_.*)$/ || next;
  $tmpl = $1;
  $type = $2;
  $tmpl =~ s|http://dbpedia.org/resource/Modèle|http://dbpedia.org/resource/Template|;
  $infobox2type{$tmpl} = $type;
}
close IB;

print STDERR "Loading links\n";
open LINKS, "<$links" || die $!;
binmode LINKS, ":utf8";
while (<LINKS>) {
  chomp;
  s/_percent_/%/g;
  s/%([0-9A-Fa-f]{2})/chr(hex($1))/eg; # uri_unescape
  $_ = Encode::decode("utf-8", $_);
  m|^<http://dbpedia.org/resource/(.*?)> <.*?> <http://fr.wikipedia.org/wiki/(.*?)>| || next;
  $dbpedia2wiki{$1} = $2;
}
close LINKS;

my ($res,$tmpl);
while (<>) {
  chomp;
  s/_percent_/%/g;
  s/%([0-9A-Fa-f]{2})/chr(hex($1))/eg;
  $_ = Encode::decode("utf8", $_);
  m|^<http://dbpedia.org/resource/(.*?)> <http://dbpedia.org/property/wikiPageUsesTemplate> <(.*?)> | || next;
  $res = $1;
  $tmpl = $2;
  $tmpl =~ s|http://dbpedia.org/resource/Modèle|http://dbpedia.org/resource/Template|;
  next if ($tmpl =~ /http:\/\/dbpedia.org\/resource\/Template:(Ouvrage|Autres_projets)$/);
  if (defined ($infobox2type{$tmpl})) {
    if (defined($dbpedia2wiki{$res})) {
      $candidate{$dbpedia2wiki{$res}}{$infobox2type{$tmpl}}++;
    } else {
      $candidate{$res}{$infobox2type{$tmpl}}++;
    }
  }
}

for $t (keys %candidate) {
  if (scalar %{$candidate{$t}} == 1) {
    for (keys %{$candidate{$t}}) {
      $line = $t."\t".$_."\n";
      $line =~ s/_/ /g;
      print $line;
    }
  }
}
