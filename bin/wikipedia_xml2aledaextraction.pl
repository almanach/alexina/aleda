#!/usr/bin/perl

use locale;
#use Encode;
use utf8;
binmode STDIN, ":utf8";
binmode STDOUT, ":utf8";
binmode STDERR, ":utf8";

$wikiname = shift || die;

while (<>) {
  chomp;
  if (/<homonymy /) {
    $skip = 1;
  } elsif (/<article /) {
    $skip = 0;
    %cats = ();
    $title = "";
    $id = 0;
    $weight = 0;
    $def = "";
    if (/ id=\"(\d+)\"/) {
      $id = $1;
    } else {
      $id = 0;
    }
    if (/ title=\"(.*?)\"/) {
      $title = $1;
    } else {
      $id = 0;
    }
    if (/ wikipedia_title=\"(.*?)\"/) {
      $wikipedia_title = $1;
    } else {
      $id = 0;
    }
  } elsif ($skip == 0) {
    if (/<category /) {
      if (/ title=\"(.*?)\"/) {
	$cats{$1} = 1;
      } else {
	$id = 0;
      }
    } elsif (/<(infobox|iwl|localization) /) {
    } elsif (/<\/?content/) {
    } elsif (/<\/(homonymy|article)>/) {
      if ($title ne "") {
	print "$wikiname$id\t$wikipedia_title\t$title\t$weight\t".(join("|",keys %cats))."\t".$def."\n" unless ($id == 0);
      }
    } else {
      s/<wl.*?>//g;
      s/<\/wl>//g;
      $weight++ if (/^ *([^ ]+ ){12}[^ ]/);
      if ($def eq "" && /[a-z]/) {
	s/ ([A-ZÉÈÀÙŠČĚŘŽÝÁÍŤĎŻĘĄ0-9])\. / \1__DOT__ /g;
	s/^(.*?\.) [A-ZÉÈÀÙŠČĚŘŽÝÁÍŤĎŻĘĄ].*/\1/g;
	s/__DOT__/./g;
	$def = $_;
      }
    }
  }
}
