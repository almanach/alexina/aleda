#!/usr/bin/perl

use locale;
#use Encode;
use utf8;
binmode STDIN, ":utf8";
binmode STDOUT, ":utf8";
binmode STDERR, ":utf8";

my $nomp_lex_file = "";

my $first = 0;
my $lang = "fr";
my $extract_variants_from_first_line = 0;

while (1) {
  $_ = shift;
  last if (!defined($_));
  if (/^$/) {last;}
  elsif (/^-f$/) {$first = shift;}
  elsif (/^-l$/) {$lang = shift;}
  elsif (/^-no_vffl$/) {$extract_variants_from_first_line = 1;}
  else {die "Unknown option: $_";}
}

open REDIRECT, ">redirections" || die "Could not open redirections";
binmode REDIRECT, ":utf8";

my ($def,$title,$ctitle,$title2,$ctitle2);
my ($lieu,$date,$verbs);
my @type2ne = ("","_PERSON","_COMPANY","_ORGANIZATION","_PRODUCT","_WORK");

lang_setup();

while (<>) {
  $l++;
  print STDERR "\t articles...\t\tline $l  \r"  if ($l % 1000 == 0);
  $skip = 0;
  chomp;
  if (/^<mediawiki.*lang=\"(..)\">/) {$lang = $1; lang_setup()}
  $orig_line = $_;
  alarm 0;
  eval {
    local $SIG{ALRM} = sub { die "timeout\n" };
    alarm 4; # So we can NEVER use "next;" for the while loop, or that alarm stuff may screw up (hence the use of $skip)

    if (/<title>(.*)<\/title>/) {
      $art++;
      $art_length = 0;
      if ($art >= $first) {
	print STDERR "Studying articles...$art   \r";
#	%hash=();
	$title = $1;
	$title2 = $title;
	$title2 =~ s/ \(.*\)$//;
	if ($title =~ /:[^ ]/) {
	  $title = "";
	}
	$type = 0;
	$ctitle = $title;
	$qmtitle = quotemeta($title2);
	$skip = 0;
      } else {
	print STDERR "Skipping articles...$art   \r";
	$skip = 1;
      }
    } elsif (/<id>(.*?)<\/id>/) {
      $id = $1;
    } else {
      $art_length++;
      if (!$skip && $art >= $first) {
	if (/<\/page>/) {
	  if ($title ne "" && $type >= 1 && $title !~ /^Liste d/) {
	    next if ($type == 1 && $ctitle =~ /^[a-zé]/);
	    #sécurité
	    $ctitle =~ s/(^|\t)[a-zàáâäçèéêëíîïñóôöùûüÿ]+(\t|$)/\t/g;
	    $ctitle =~ s/(^|\t)[a-zàáâäçèéêëíîïñóôöùûüÿ]+(\t|$)/\t/g;
	    $ctitle =~ s/\t+$//;
	    $ctitle =~ s/^\t+//;
	    next if $ctitle eq "";
	    #fin sécurité
	    print "frwiki$id\t$ctitle\t";
	    print $type2ne[$type]."\t$art_length\t";
	    print "# $def\t\thttp://fr.wikipedia.org/wiki/$title\n";
	  }
	  $def = "";
	  $bool = 0;
	  $skip = 1;
	  $in_open_brackets = 0;
	  $beginning = "";
	  #      print "$title\t$odef\t$def\n" unless $title eq "";
	} elsif ($def eq "" && s/^.*<text.*?>(?:&lt;references\/&gt;)?//) {
	  $bool = 1;
	  $in_open_brackets = 0;
	  $beginning = "";
	} elsif ($type != -1 && /^\[\[Catégorie:(?:Naissance|Décès|Personnalité|Scientifique|Ingénieur|Membre|Chanteur|Joueur|Chevalier|Consultant|Polytechnicien|Écrivain|Réalisateur|Député|Poète|Dramaturge|Compositeur|Designer|Linguiste|Normalien|Inventeur|Chimiste|Archéologue|Philosophe|Lauréat|Ministre|Pape|Président|Peintre|Mathématicien|Scénariste|Physicien|Monarque|Actrice|Femme|Homme)[ \]]/i) {
	  $type = 1;
	} elsif ($type == 0 && /^\[\[Catégorie:(?:Entreprise|Filiale|Groupe de médias|Société ayant son siège|Société cotée)[ \]]/i) {
	  $type = 2;
	} elsif ($type == 0 && /^\[\[Catégorie:(?:Organisations?|Organismes?|Partis?|Académies?|Universités?|Titres? de presse|Associations?|Radio|Organe législatif|Agence de presse|Institutions?|Juridiction administrative|Juridiction d'appel|Juridiction de l'Ancien Régime|Juridiction judiciaire française|Administration|Club)[ \]]/i && $_ !~ /^\[\[Catégorie:(?:Organisme modèle)[ \]]/i) { # Académie Université
	  $type = 3;
	} elsif ($type == 0 && /^\[\[Catégorie:(?:Logiciel|Marque)[ \]]/i) { # Système
	  $type = 4;
	} elsif ($type == 0 && /^\[\[Catégorie:(?:Film|Roman)[ \]]/i) { # Série
	  $type = 5;
	} elsif (/^\[\[Catégorie:(?:Liste|Sigle)[ \]]/i) {
	  $type = -1;
	} elsif ($title eq "" || $title =~ /:[^ ]/) {
	  $skip = 1;
# 	} else { # we parse the article to gather capitalization information about the title
# 	  unless ($title =~ /^.$/) { # because then it's useless and takes a lot of time
# 	    $old = $_;
# 	    while (s/^.*?\b($qmtitle)\b//i) {
# 	      $hash{$1}++;
# 	    }
# 	    $_ = $old;
# 	  }
	}
      } else {
	$skip = 1;
      }
    }
    if ($skip == 0) {
      s/\[\[\]\]//g;
      if (/\{\{[^\{\}]*(škrbin|stub|ébauche)/) {
	s/^(.*?)\{\{[^\{\}]*(?:škrbin|stub|ébauche).*?\}\}/\1/; # capturer et remettre \1 ne sert à
                                                                # rien, mais empêche une boucle
                                                                # infinie sur un cas particulier du
                                                                # dump wikipedia français téléchargé
                                                                # début février 2010
      }
      if (/\#redirect \[\[(.*?)\]\]/i) {
	$skip = 1;
	print REDIRECT "$title\t$1\n" unless $title eq "";
	$title = "";
      } elsif (/\{\{homonymie\}\}/) {
	$title = "";
	$skip = 1;
      } elsif ($bool && $title ne "" && $def eq "") {
	$_ = $beginning.$_;
	$beginning = "";
	if (/^\[\[(Image|Slika|Soubor):/) {
	  while (s/^\[\[[^\[\]]*\[\[[^\[\]]*\]\]/\[\[/) {}
	  while (s/^\[\[[^\[]*\[[^\[\]]*\]/\[\[/) {}
	  s/^\[\[[^\]]+\]\]+//;
	  s/^[^\[\]]*\]\]+//;
	}
	if (/^\s*$/
	    || /^:/
	    || /Voir Homonymes/i
	    || /\{\{redirect/i
	    || /^\&lt;!--/) {
	  $skip = 1; # inutile mais bon...
	} else {
	  if (/^\s*\{\{[^\}]*$/) {
	    $in_open_brackets++;
	    $skip = 1;
	  } else {
	    if ($in_open_brackets > 0) {
	      if (/^[^\{]*\}\}/) {
		$in_open_brackets = 0;
	      }
	      $skip = 1;
	    } elsif (/^\s*(?:\{\{[^\{\}]*\}\})?$/) {
	      $skip = 1;
	    }
	  }
	  if (!$skip && !$in_open_brackets && (/..\[[^\]]*$/ || /..\{[^\}]*$/)) {
	    $beginning = $_;
	    $skip = 1;
	  }
	  if (!$skip) {
	    s/.lt;ref.*?gt.*?lt.\/ref.gt.//g;
	    s/(av|st|St)\./$1\__DOT__/g;
	    while (s/(\[\[[^\]]*)\./$1\__DOT__/g) {}
	    while (s/(\{\{[^\}]*)\./$1\__DOT__/g) {}
	    $def = $_;
	    $def =~ s/^(.*?[^ ][^ ])\. *(?: ([A-Z]|É|À|È|Ç|Č|Š|Ž|Ň|Ť|Ď|Ľ|Á|Í|Ó|Ú|Ý).*|$)/\1/;
	    $def =~ s/__DOT__/./g;
	    $odef = $def;
	    $def =~ s/\s*&lt;\s*br\s*&gt;\s*/ /g;
	    $def =~ s/  +/ /g;
	    $def =~ s/^ //;
	    $def =~ s/’/'/g;
	    $def =~ s/{{\!}}//g;
	    $def =~ s/{{PAGENAME,?}}+/PAGENAME/g;
	    $def =~ s/\{[\{\[]date\|(.*?)\|(.*?)\|(.*?)\}\}/\1 \2 \3/gi;
	    $def =~ s/\{[\{\[]linktext.*?\}\}//gi;
	    $def =~ s/\{[\{\[]formatnum:(.*?)\}\}/\1/gi;
	    $def =~ s/\{[\{\[]pron[ou]nciation\|(.*?)\}\}//gi;
	    $def =~ s/\{[\{\[]IPA\|(.*?)\}\}//gi;
	    $def =~ s/\{[\{\[][a-z ]+\|([^|{}]+)\|.*?\}\}/\1/gi;
	    $def =~ s/\{[\{\[]lang-[a-z]+\|([^|{}]+)\}\}/\1/gi;
	    $def =~ s/\{[\{\[]zh-[a-z]+\|.*?\|p=[^\|]*\}\}//gi;
	    $def =~ s/[\(\（][^\(\)\（\）]*?[\)\）]//g;
	    $def =~ s/[\(\（][^\(\)\（\）]*?[\)\）]//g;
	    $def =~ s/&lt;!--.*?--&gt;//g;
	    if ($def =~ /&lt;small&gt;/ && $def =~ /&lt;\/small&gt;/) { # sinon on boucle...
	      $def =~ s/&lt;small&gt;.*?&lt;\/small&gt;//g;
	    }
	    $def =~ s/, ,/,/g;
	    $def =~ s/''' '''/ /g;
	    $def =~ s/'''  '''/ /g;
	    $def =~ s/'''   '''/ /g; # avec " +", ça boucle sur une phrase, sans aucune raison apparente...
	    $def =~ s/\&amp;/\&/g;
	    $def =~ s/\&nbsp;/ /g;
	    $def =~ s/  +/ /g;
	    $def =~ s/^ //;
	    $def =~ s/ $//;
	    $def =~ s/\[\[([^\|\[\]]*)\](?!\])/$1/g; # nettoyage...
	    $def =~ s/(?<!\[)\[([^\|\[\]]*)\]\]/$1/g; # nettoyage...
	    if ($def=~/^\s*$/ || $def =~ /^\s*\*/ || $def =~ /^\[\[\]\]$/) {
	      $skip = 1;
	    } else {
	      $skip = 1 unless $def =~ s/^(?:|.*?(?:[ \[]|[Ll]'))'''+(?!')//;
	      if (!$skip) {
		$def =~ s/^([^\[]+)'''([^\[\]]*)\]\]/$1$2/;
		while ($def =~ s/^(.*?\[\[[^\]]*)'''([^\[]+?)'''([^\[\]]*\]\])/$1$2$3$4/g) {}
		if ($extract_variants_from_first_line) {
		  if ($def =~ s/^([^'](?:.*[^'])?)'''+(?:\&quot;|,|:)? //) {
		    $ctitle = $1;
		    #      $ctitle = decode("iso-8859-1", encode("iso-8859-1", $ctitle));
		    $ctitle =~ s/^L'// unless $title =~ /^l'/i;
		    while ($ctitle =~ s/^(.*?)'''+.*?'''+/$1\t/) {
		    }
		    $ctitle =~ s/\(.*?\) *//g;
		    $ctitle = clean ($ctitle);
		    $ctitle =~ s/ '+$//; # security (found example...)
		    $ctitle =~ s/PAGENAME(\t|$)//;
		    $ctitle =~ s/\t$//;
		    # 		  for $t (keys %hash) {
		    # 		    if ($hash{$t} > $hash{$title2}) {
		    # 		      $title2 = $t;
		    # 		    }
		    # 		  }
		    unless ($title2 eq "" || $ctitle =~ /^[a-zé]/) {
		      #		    if ($hash{lc($title2)} > $hash{$title2}*0.1) {
		      #		      $title2 = lc($title2);
		      #		    }
		      ###		    $title2 =~ s/ \(.*?\)//g;
		      $qmtitle2 = quotemeta($title2);
		      if ($ctitle !~ /(^|\t)$qmtitle2(\t|$)/i) {
			if ($ctitle =~ /^[^ ]*(?:[A-Z]|É|À|È|Ç|Č|Š|Ž|Ň|Ť|Ď|Ľ|Á|Í|Ó|Ú|Ý)/) {
			  $ctitle = $title2."\t".$ctitle;
			  $ctitle =~ s/\t$//;
			} else {
			  $ctitle = $title2;
			}
		      }
		    }
		  } else {
		    $title2 =~ s/ \(.*?\)//g;
		    $ctitle = $title2;
		  }
		} else {
		  $def =~ s/^([^'](?:.*[^'])?)'''+(?:\&quot;|,|:)? //;
		}
	      }
	    }
	  }
	  if ($def=~/^\s*$/ || $def =~ /^\s*\*/ || $def =~ /^\[\[\]\]$/) {
	    $skip = 1;
	  }
	  if (!$skip) {
	    $def =~ s/^, +//;
	    $def =~ s/^ *[\(\（] *//;
	    $def =~ s/^[^\{\}]*(?:\{\{[^\{\}]*\}\}[^\{\}]*)?\}\} *//;
	    $def =~ s/^[^\[\]]*(?:\[\[[^\[\]]*\]\][^\[\]]*)?\]\] *//;
	    $def =~ s/–/-/g;
	    if ($lang eq "fr") {
	      $def =~ s/^$verbs(?:,? en \[\[.*?\]\],?)? (les? |la |une? |l\'|des? )?/$2/i
	    } elsif ($lang eq "sl") {
	      $def =~ s/^$verbs(?:,? v \[\[.*?\]\] (?:in \[\[.*?\]\])?,?)? //i
	    } elsif ($lang eq "cs") {
	      $def =~ s/^$verbs(?:,? v \[\[.*?\]\] (?:a \[\[.*?\]\])?,?)? //i
	    } elsif ( $lang eq "en") {
	      $def =~ s/^$verbs(?:,? in \[\[.*?\]\] (?:and \[\[.*?\]\])?,?)? (an? |the )?/$2/i
	    }
	    $def = clean ($def);
	    $def =~ s/^(les? |la |une? |l\'|des? )// unless $def =~ /^une? des/;
	    if ($def =~ s/(?:, )?née?$lieu? $date$lieu?(?:,| et) (?:morte?|décédée?)$lieu? $date$lieu?,? *$verbs(?:,? en .*?,?)? (les? |la |une? |l\'|des? )?//
		||
		$def =~ s/(?:, )?née?$lieu? $date$lieu?(?:,| et) (?:morte?|décédée?)$lieu? $date$lieu?//
		||
		$def =~ s/(?:, )?née?$lieu? $date$lieu?,?()()()() *$verbs(?:,? en .*?,?)? (les? |la |une? |l\'|des? )?//
		||
		$def =~ s/(?:, )?née?$lieu? $date$lieu?,?()()()()//
		||
		$def =~ s/()()()()(?:, )?(?:morte?|décédée?)$lieu? $date$lieu?,? *$verbs(?:,? en .*?,?)? (les? |la |une? |l\'|des? )?//
		||
		$def =~ s/()()()()(?:, )?(?:morte?|décédée?)$lieu? $date$lieu?,?//
	       ) {
	      $def .= " (";
	      if ("$1$4" ne "") {$def .= "$1$4, "}
	      $def .= "$2 $3";
	      if ($7 ne "") {$def .= " — "}
	      if ("$5$8" ne "") {$def .= "$5$8, "}
	      if ($7 ne "") {$def .= "$6 $7"}
	      $def .= ")";
	    }
	    $bool = 0;
	  } else {
#	    $def = clean ($def);
	    $def = "";
	  }
	}
      }
    }
  };
  if ($@ and $@ =~ /timeout/) {
    print STDERR "Studying articles...\t\tline $l\t(screwed up)\t$title\t$orig_line\n";      
    $title = "";
    $def = "";
  }
  alarm 0;
}

close REDIRECT;

print STDERR "\ndone\n";

sub clean {
  my $s = shift;
  $s =~ s/\[\[(.[^\|]*?)\]\]/$1/g;
  $s =~ s/\[\[.*?\|(.*?)\]\]/$1/g;
  $s =~ s/\{\{(.[^\|]*?)\}\}/$1/g;
  $s =~ s/\{\{.*?\|(.*?)\}\}/$1/g;
  $s =~ s/\'\'+//g;
  $s =~ s/\&quot;/'/g;
  $s =~ s/  +/ /g;
  $s =~ s/ $//;
  $s =~ s/(, )+/, /g;
  $s =~ s/, $//g;
  $s =~ s/ ,/,/g;
  return $s;
}

sub lang_setup {
  if ($lang eq "fr") {
    $lieu = qr/(?: +(?:(?:à|au|en)|dans le \d+e arrondissement de) ([^ \d][^ ,]*))/o;
    $date = qr/(?:le (\d{1,2} (?:é|[a-zA-Z])+)|en) (-?\d{1,4}(?:av\. J\.-C\.)?)/o;
    $verbs = qr/(?:est|était|forment|forme|sont|recouvre|désigne|était|étaient|fut|furent)/;
  } elsif ($lang eq "sl") {
    $verbs = qr/(?:navadno )?(?:so|je|označuje)/o;
  } elsif ($lang eq "cs") {
    $verbs = qr/(?:navadno )?(?:jsou|je|označuje|byla?)/o;
  } elsif ( $lang eq "en") {
    $verbs = qr/(?:is|was|were)/o;
  } else {
    $lieu = qr/(?:zzzzzzzzzzzzzzzzz)/o;
    $date = qr/(?:zzzzzzzzzzzzzzzzz)/o;
    $verbs = qr/(?:zzzzzzzzzzzzzzzzz)/;
  }
}
