#!/usr/bin/perl

#use locale;
#use Encode;
use utf8;
binmode STDIN, ":utf8";
binmode STDOUT, ":utf8";
binmode STDERR, ":utf8";

$wikiname = shift || die;

while (<>) {
  chomp;
  $l++;
  if ($l % 10000 == 0) {print STDERR "Line $l\r"}
  if (/<article /) {
    if (/id=\"(.*?)\"/) {
      $id = $1;
      if (/wikipedia_title=\"(.*?)\"/) {
	$wikipedia_title2id{$1} = $id;
      }
    }
    $skip = 1;
  } elsif (/<\/article/) {
    $skip = 0;
  } elsif ($skip == 0) {
    if (/<homonymy /) {
      $title = "";
      if (/ title=\"(.*?)\"/) {
	$title = $1;
      }
      $title =~ s/^(.*[^\s])\s*\(.*\)$/\1/;
      if (length($title) == 1) {$title = ""}
    } elsif (/<category /) {
      if (/ title=\"(.*?)\"/) {
	$cats{$1} = 1;
      } else {
	$id = 0;
      }
    } elsif (/<iwl /) {
    } elsif (/<\/?content/) {
    } elsif (/<\/homonymy>/) {
    } elsif ($title ne "") {
      next unless "<wl ";
      if ($title =~ /^[A-ZÉÀÈÂÊÎÔÛ]{2,}$/) {
	$is_acronym = 1;
      } else {
	$is_acronym = 0;
      }
      if ($is_acronym == 0) {
	$qmtitle = lc($title);	
      } else {
	$qmtitle = $title;
      }
      $qmtitle = quotemeta($qmtitle);
      $qmtitle =~ s/E/[EÉ]/g;
      $qmtitle =~ s/e/[eé]/g;
      $qmtitle_nolc = $title;
      $qmtitle_nolc = quotemeta($qmtitle_nolc);
      $xqmtitle_nolc =~ s/E/[EÉ]/g;
      $xqmtitle_nolc =~ s/e/[eé]/g;
      $xqmtitle = quotemeta(lc($title));
      $sxqmtitle = $xqmtitle;
      $xqmtitle =~ s/E/[EÉ]/g;
      $xqmtitle =~ s/e/[eé]/g;
      $xqmtitle =~ s/(\\.|[^\\])(?=.)/\1.*?/g;
      $sxqmtitle =~ s/(\\.|[^\\])(?=.)/[ -']\1.*?/g;
      $sxqmtitle =~ s/^\[ -'\]//;
      $sxqmtitle =~ s/E/[EÉ]/g;
      $sxqmtitle =~ s/e/[eé]/g;
      if ($is_acronym == 0) {
	$qmtitle = qr/$qmtitle/i;
      } else {
	$qmtitle = qr/$qmtitle/;
      }
      $xqmtitle = qr/$xqmtitle/i;
      $sxqmtitle = qr/$sxqmtitle/i;
      %candidates = ();
      $candidates_nb = 0;
      while (s/^.*?<wl title=\"(.*?)\".*?<\/wl>//) {
	$candidate = $1;
	next if $candidate =~ /#/;
	if ($candidate =~ /^$qmtitle(?: \(.*\))?$/) {
	  $candidates_nb++ unless defined($candidates{$candidate});
	  $candidates{$candidate} += 10;
	} elsif ($candidate =~ /^$qmtitle / && $title =~ / /) { # évitons les pages d'homonymie de prénoms
	  $candidates_nb++ unless defined($candidates{$candidate});
	  $candidates{$candidate} += 5;
	} elsif ($candidate =~ / $qmtitle(?: \(.*\))?$/ && $title =~ / /) {
	  $candidates_nb++ unless defined($candidates{$candidate});
	  $candidates{$candidate} += 5;
	} elsif ($candidate =~ / $qmtitle_nolc(?: \(.*\))?$/ && $title !~ / /) {
	  $candidates_nb++ unless defined($candidates{$candidate});
	  $candidates{$candidate} += 5;
	} elsif ($candidate =~ /\W$qmtitle\W/ && $title =~ / /) {
	  $candidates_nb++ unless defined($candidates{$candidate});
	  $candidates{$candidate} += 3;
	} elsif ($candidate =~ /^$qmtitle /
		 || $candidate =~ / $qmtitle(?: \(.*\))?$/) {
	} else {
	  if ($candidate =~ /$sxqmtitle/) {
	    $candidates_nb++ unless defined($candidates{$candidate});
	    $candidates{$candidate} += 2;
#	  } elsif ($is_acronym == 0 && $candidate =~ /^$xqmtitle$/) {
#	    $candidates_nb++ unless defined($candidates{$candidate});
#	    $candidates{$candidate} += 1;
	  }
	}
#	print STDERR "$title\t$candidate\t$candidates{$candidate}\n" if ($title =~ /^(MNR)$/);
      }
      if ($candidates_nb > 0) {
	@candidates = sort {$candidates{$b} <=> $candidates{$a}} keys %candidates;
	$best_score = $candidates{$candidates[0]};
	for my $i (0..$#candidates) {
	  last if ($candidates{$candidates[$i]} < $best_score);
	  $hash{$title}{$candidates[$i]} = $candidates{$candidates[$i]};
#	  print STDERR "\$hash{$title}{$candidates[$i]} = $candidates{$candidates[$i]};\n" if ($title =~ /^(MNR)$/);
	}
      }
    }
  }
}

for $title (keys %hash) {
  for $redirtitle (keys %{$hash{$title}}) {
    if (defined($wikipedia_title2id{$redirtitle})) {
      $title =~ s/ \(.*\)$//;
      print "$title\t$wikiname$wikipedia_title2id{$redirtitle}\n";
    }
  }
}
