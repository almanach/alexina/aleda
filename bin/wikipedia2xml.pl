#!/usr/bin/perl

use locale;
#use Encode;
use utf8;
binmode STDIN, ":utf8";
binmode STDOUT, ":utf8";
binmode STDERR, ":utf8";

my $first = 0;
my $lang = "fr";

while (1) {
  $_ = shift;
  last if (!defined($_));
  if (/^$/) {last;}
  elsif (/^-f$/) {$first = shift;}
  else {die "Unknown option: $_";}
}

my ($def,$title,$ctitle,$title2,$ctitle2);

while (<>) {
  $l++;
  print STDERR "\t articles...\t\tline $l  \r"  if ($l % 1000 == 0);
  $skip = 0;
  chomp;
  $orig_line = $_;
  alarm 0;
  eval {
    local $SIG{ALRM} = sub { die "timeout\n" };
    alarm 4; # So we can NEVER use "next;" for the while loop, or that alarm stuff may screw up (hence the use of $skip)
    if (/^<mediawiki.* xml:lang="(.*?)"/) {
      $lang = $1;
    } elsif (/<title>(.*)<\/title>/) {
      $art++;
      $id = 0;
      if ($art >= $first) {
	print STDERR "Studying articles...$art   \r";
	%hash=();
	%cats = ();
	%interwikilinks = ();
	@infoboxes = ();
	$title = $1;
	$title2 = $title;
	$title2 =~ s/ \((.*)\)$//;
	if ($lang eq "es" && $1 eq "desambiguación") {
	  $is_homonymy = 1;
	}
	if ($title =~ /:[^ ]/) {
	  $title = "";
	}
	$qmtitle = quotemeta($title2);
	$skip = 0;
	$innavbox = 0;
	$incontent = 0;
	$content = "";
	$long = "";
	$lat = "";
	$superf = "";
      } else {
	print STDERR "Skipping articles...$art   \r";
	$skip = 1;
      }
    } elsif ($id == 0 && /<id>(.*)<\/id>/) {
      $id = $1;
    } else {
      if (!$skip && $art >= $first) {
	if (/<\/page>/) {
	  if ($title ne "") {
	    $modtitle = (sort {$hash{$b} <=> $hash{$a}} keys %hash)[0];
	    if ($modtitle eq "") {$modtitle = $title} # sécurité, des pbs ont été trouvés
	    if ($is_homonymy) {
	      print "<homonymy id=\"$id\" title=\"$modtitle\" wikipedia_title=\"$title\">\n";
	    } else {
	      print "<article id=\"$id\" title=\"$modtitle\" wikipedia_title=\"$title\">\n";
	    }
	    for (keys %cats) {
	      print "  <category title=\"$_\" lang=\"$lang\"/>\n" unless /^\s*$/;
	    }
	    for (0..$#infoboxes) {
	      print "  <infobox title=\"$infoboxes[$_]\" lang=\"$lang\"/>\n" unless $infoboxes[$_] =~ /^\s*$/;
	    }
	    for (keys %interwikilinks) {
	      next if ($_ =~ /^\s*$/ || $interwikilinks{$_} =~ /^\s*$/);
#	      next unless ($_ =~ /^(en|sl|fr)$/);
	      print "  <iwl title=\"$interwikilinks{$_}\" lang=\"$_\"/>\n";
	    }
	    if ($long ne "" && $lat ne "") {
	      print "  <localization latitude=\"$lat\" longitude=\"$long\"/>\n";
	    }
	    print "  <content lang=\"$lang\">\n";
	    $content =~ s/\n*$/\n/;
	    print $content;
	    print "  </content>\n";
	    if ($is_homonymy) {
	      print "</homonymy>\n";
	    } else {
	      print "</article>\n";
	    }
	  }
	  $def = "";
	  $bool = 0;
	  $skip = 1;
	  $is_homonymy = 0;
	  $in_open_brackets = 0;
	  #      print "$title\t$odef\t$def\n" unless $title eq "";
	} elsif ($art >= $first) {
#	  print STDERR "$_\n";
	  if ($title eq "" || $title =~ /:[^ ]/) {
	    $skip = 1;
	  } else { # we parse the article to gather capitalization information about the title
	    unless ($title =~ /^.$/) { # because then it's useless and takes a lot of time
	      $old = $_;
	      while (s/^.*?\b($qmtitle)\b//i) {
		if ($1 eq $title) {
		  $hash{$1} += 0.5;
		} else {
		  $hash{$1}++;
		}
	      }
	      $_ = $old;
	    }
	  }
	}
      } else {
	$skip = 1;
      }
    }
#    print STDERR "$skip - $_\n";
    if (!$skip) {
      if (/\{\{[^\{\}]*(škrbin|stub|ébauche|esbozo)/i) {
	s/\{\{[^\{\}]*(škrbin|stub|ébauche|esbozo)[^\{\}]*\}\}//i;
      }
#      print STDERR "|$_|\n";
      s/\s*<\/text>$//;
      if (/\#redirect/i) {
	$title = "";
	$skip = 1;
      } elsif (   ($lang eq "fr" && /^\[\[Catégorie:\s*(.*?)\s*(\]\]|\|)/i)
		  || ($lang eq "en" && /^\[\[Category:\s*(.*?)\s*(\]\]|\|)/i)
		  || ($lang eq "nl" && /^\[\[Categorie:\s*(.*?)\s*(\]\]|\|)/i)
		  || ($lang eq "de" && /^\[\[Kategorie:\s*(.*?)\s*(\]\]|\|)/i)
		  || ($lang eq "sl" && /^\[\[Kategorija:\s*(.*?)\s*(\]\]|\|)/i)
		  || ($lang eq "es" && /^\[\[Categoría:\s*(.*?)\s*(\]\]|\|)/i)
	      ) {
	$cats{$1} = 1 unless length($1) <= 1;
      } elsif (/^==+ ?(?:liens? externes?|external links?|voir aussi|see also|zie ook|externe links|notes? (?:et|and) r[ée]f[ée]rences?|notes?|r[eé]f[eé]rences?|zunanje povezave|zunanja povezava|glej tudi|enlaces? externos?|bibliografía|referencias|véase también|weblinks|siehe auch|referenzen|literatur|einzelnachweise) ?==+/i) {
	$skip = 1;
      } elsif (/^\[\[((?:[a-z]{2,3}(?:-[a-z-]+)?|simple):.*)\]\]$/) {
	my $links = $1;
	for my $link (split /\|(?=(?:[a-z]{2,3}(?:-[a-z-]+)?|simple):)/, $links) {
	  $link =~ /^\s*([a-z]{2,3}(?:-[a-z-]+)?|simple)\s*:\s*(.*?)\s*$/ || next;
	  $interwikilinks{$1} = $2;
	}
	$skip = 1;
      } elsif (/^\s*<text .*?>[\s\']*{{[^}]+}}$/) {
	$incontent = 1;
	if (/\{\{homonym(?:ie|y)\}\}/i || /\{\{begriffsklärung\}\}/i) {
	  $is_homonymy = 1;
	}
      } elsif (/^\s*<text .*?>[\s\']*\{\{(?:Ficha de|infobox)[\s_]+(.*?)[\s\|]*$/i) {
	$incontent = 1;
	$innavbox = 1;
	push @infoboxes, $1;
      } elsif (/\{\{homonym(?:ie|y)\}\}/i) {
	$is_homonymy = 1;
      } elsif (/^{{([^}]|[^}])+}}$/) {
      } elsif (/^\s*<text .*?>\[\[(Fichier|File|Slika|Image|Soubor|Archivo|Bestand|Datei):.*\]\]$/) {
	$incontent = 1;
      } elsif (/^\[\[(Fichier|File|Slika|Image|Soubor|Archivo|Bestand|Datei):.*\]\]$/) {
      } elsif (/^\s*<text .*?>{\|/ || /^\s*<text .*?>{{/) {
	$innavbox = 1;
	$incontent = 1;
      } elsif (/^{\|/ || /^{{[^}]*$/) {
	$innavbox = 1;
	if (/^\{\{(?:Ficha de|infobox)[\s_]+(.*?)[\s\|]*$/i) {
	  push @infoboxes, $1;
	}
      } elsif ($innavbox && (/^\|}/ || /^}}/ || (!/{{/ && /}}/))) {
	$innavbox = 0 unless (/}}\s*&lt;\/ref&gt;/ && !/}}\s*&lt;\/ref&gt;.*}}/);
      } elsif ($title ne "" && /^\s*\|+\s*(?:coordenadas)/) {
	if (/{{coord\|(.*?)\|(.*?)\|(.*?)\|[NS]\|(.*?)\|(.*?)\|(.*?)\|[EOW]}}/) {
	  $lat = $1 + $2/60 + $3/3600;
	  $long = $4 + $5/60 + $6/3600;
	}
      } elsif ($title ne "" && /{{Coordinate/) {
	if (/{{Coordinate\|text=DMS\|article=\/\|NS=(.*?)\/(.*?)\/(.*?)\/([NS])\|EW=(.*?)\/(.*?)\/(.*?)\/([EOW])[\|\}]/) {
	  $lat = $1 + $2/60 + $3/3600;
	  if ($4 eq "S") {$lat = -$lat}
	  $long = $5 + $6/60 + $7/3600;
	  if ($8 eq "O" || $8 eq "W") {$long = -$long}
	}
      } elsif ($title ne "" && /^\s*\|+\s*(?:longitude|latitude|superficie|km)/ && $innavbox == 1 && !/à/) {
	s/{{coord\/dms2dec\|(.)\|(.*?)}}/\2\|\1/;
	if (/^\s*\|+\s*(?:latitude|NS)\s*=\s*([-0-9\/\|NS\.]+)/) {
	  $lat = $1;
	  if ($lat =~ /^(\d+)(?:[\/\|](\d+)(?:[\/\|](\d+))?)?[\/\|]([NS])$/) {
	    $lat = $1 + $2/60 + $3/3600;
	    if ($4 eq "S") {$lat = -$lat}
	  } elsif ($lat !~ /^-?\d+(\.\d+)?$/) {
	    $lat = "";
	  }
	} elsif (/^\s*\|+\s*(?:longitude|EW)\s*=\s*([-0-9\/\|EOW\.]+)/) {
	  $long = $1;
	  if ($long =~ /^(\d+)(?:[\/\|](\d+)(?:[\/\|](\d+))?)?[\/\|]([EOW])$/) {
	    $long = $1 + $2/60 + $3/3600;
	    if ($4 eq "O" || $4 eq "W") {$long = -$long}
	  } elsif ($long !~ /^-?\d+(\.\d+)?$/) {
	    $long = "";
	  }
	} elsif (/^\s*\|+\s*(?:superficie|km.)\s*=\s*([0-9\.]+)/) {
	  $superf = $1;
	}
      } elsif ($title ne "" && $innavbox == 0 && ($incontent || s/<text .*?>//)) {
	$incontent = 1;
	s/(<|&lt;)!--.*?--(>|&gt;)//g;
	if (s/{{([^}]|}[^}])+$//) {$innavbox = 1}
	if (/^\[\[(Fichier|File|Image|Slika|Soubor|Archivo|Bestand|Datei):/) {
	  while (s/^\[\[[^\[\]]*\[\[[^\[\]]*\]\]/\[\[/) {}
	  while (s/^\[\[[^\[]*\[[^\[\]]*\]/\[\[/) {}
	  s/^\[\[[^\]]+\]\]+//;
	  s/^[^\[\]]*\]\]+//;
	}
	if (/^\s*$/
	    || /^:/
	    || /Voir Homonymes/i
	    || /\{\{redirect/i
	    || /^\&lt;!--/) {
	  $skip = 1; # inutile mais bon...
	} elsif (!$skip) {
	  s/&lt;ref.*?&gt;.*?&lt;\/ref&gt;//g;
	  s/&lt;ref.*?&gt;.*//g;
	  s/.*?&lt;\/ref&gt;//g;
	  s/  +/ /g;
	  s/^ //;
	  s/’/'/g;
	  s/[“”]/"/g;
#	  print STDERR "0: $_\n";
	  s/{{\!}}//g;
	  s/{{PAGENAME}}/PAGENAME/g;
	  s/\[https?:\/\/.*? (.*?)\]/\1/g;
	  s/^\* \{\{[a-z]{2,3}\}\} *//;
	  s/{{[^}]*(\|[^}]*){4,}}}//g;
	  s/\{[\{\[]date\|(.*?)\|(.*?)\|(.*?)\}\}/\1 \2 \3/gi;
	  s/\{[\{\[]linktext.*?\}\}//gi;
	  s/\{[\{\[]formatnum:(.*?)\}\}/\1/gi;
	  s/\{[\{\[]pron[ou]nciation\|(.*?)\}\}//gi;
	  s/\{[\{\[]IPA\|(.*?)\}\}//gi;
	  s/\{[\{\[](?:[a-z ]+|monotonic|polytonic)\|([^|{}]+)(?:\|.*?)?\}\}/\1/gi;
	  s/\{[\{\[]lang-[a-z]+\|([^|{}]+)\}\}/\1/gi;
	  s/\{[\{\[]zh-[a-z]+\|.*?\|p=[^\|]*\}\}//gi;
#	  print STDERR "1: $_\n";
#	  s/[\(\（][^\(\)\（\）]*?[\)\）]//g;
#	  s/[\(\（][^\(\)\（\）]*?[\)\）]//g;
	  s/\&amp;/\&/g;
	  s/\&nbsp;/ /g;
	  s/  +/ /g;
	  s/^, +//;
	  s/^ *[\(\（] *//;
	  s/^[^\{\}]*(?:\{\{[^\{\}]*\}\}[^\{\}]*)?\}\} *//;
	  s/^[^\[\]]*(?:\[\[[^\[\]]*\]\][^\[\]]*)?\]\] *//;
	  if ($no_wl == 1) {
	    s/\[\[([^\|]*?)\]\]/$1/g;
	    s/\[\[.*?\|(.*?)\]\]/$1/g;
	  } else {
	    s/\[\[([^\|]*?)\]\]/#_#_#wl title=\"$1\" lang=\"$lang\"_#_#_$1#_#_#\/wl_#_#_/g;
	    s/\[\[(.*?)\|(.*?)\]\]/#_#_#wl title=\"$1\" lang=\"$lang\"_#_#_$2#_#_#\/wl_#_#_/g;
	  }
#	  print STDERR "2: $_\n";
	  s/\{\{([^\|]*?)\}\}/$1/g;
#	  print STDERR "3: $_\n";
	  s/\{\{.*?\|(.*?)\}\}/$1/g;
	  s/\'\'\'\'/\'/g;
	  s/\'\'+//g;
	  s/  +/ /g;
	  s/ $//;
	  s/ ,/,/g;
	  s/\&quot;/'/g;
	  s/\&lt;/</g;
	  s/\&gt;/>/g;
	  s/\&ndash;/--/g;
	  while (s/<([^>]*?)(?: [^>]*?)?>(.*?)<\/\1>/\2/g) {}
	  s/<\/?references.*?>//;
	  s/<\/?revision.*?>//;
	  s/^--+ ?//;
	  s/#_#_#/</g;
	  s/_#_#_/>/g;
	  s/^\#[:\*]? ?//;
	  s/^\*+ ?//;
	  s/^; ?//;
	  s/^=+ ?(.*?) ?=+$/\n\1/;
	  s/<br *\/*>/\n\n/g;
	  s/<div *\/*>/\n/g;
	  s/<div .*?>/\n/g;
	  s/<p *\/*>/\n/g;
	  s/<p .*?>/\n/g;
#	  print STDERR "4: $_\n";
	  $content .= $_."\n";
	}
#      } else {
#	print STDERR "$title ne \"\" && $innavbox == 0 && ($incontent || ".(s/<text .*?>// || "0")."))\t$_\n";
      }
    }
  };
  if ($@ and $@ =~ /timeout/) {
    print STDERR "Studying articles...\t\tline $l\t(screwed up)\t$title\t$orig_line\n";      
    $title = "";
    $def = "";
  }
  alarm 0;
}
print STDERR "\ndone\n";
