#!/usr/bin/perl

use utf8;
binmode STDIN, ":utf8";
binmode STDOUT, ":utf8";
binmode STDERR, ":utf8";

$data = shift || die;
open DATA, "<$data" || die $!;
binmode DATA, ":utf8";
while (<DATA>) {
  chomp;
  /^(.+?)\t(.+)$/ || next;
  $pagename = $1;
  $type = $2;
  $pagename =~ s/_/ /g;
  $pagename2type{$pagename} = $type;
}

while (<>) {
  chomp;
  /^.{2,3}wiki\d+\t(.*?)\t.*?\t(?:.*?)\t(.*?)\t/ || next;
  $pagename = $1;
  $categories = $2;
  for $category (split (/\|/, $categories)) {
    $category2occ{$category}++;
    if (defined($pagename2type{$pagename})) {
      $type = $pagename2type{$pagename};
      $category2type{$category}{$type}++;
      $category2knownocc{$category}++;
    }
  }
}

for $category (sort {$category2occ{$b} <=> $category2occ{$a}} keys %category2type) {
  for $type (sort {$category2type{$category}{$b} <=> $category2type{$category}{$a}} keys %{$category2type{$category}}) {
    next if ($category2type{$category}{$type} < 0.75*$category2knownocc{$category});
    next if ($category2type{$category}{$type} <= 1);
    next if ($category2knownocc{$category} < 4 && $category2knownocc{$category} <= 0.5*$category2occ{$category});
    next if ($category2knownocc{$category} < 10 && $category2knownocc{$category} <= 0.3*$category2occ{$category});
    if ($category2knownocc{$category} < 30) {
      next if ($category2knownocc{$category} <= 0.15*$category2occ{$category});
      next if ($category2type{$category}{$type} < 0.9*$category2knownocc{$category} && $category2knownocc{$category} <= 0.3*$category2occ{$category});
    }
    next if ($category2knownocc{$category} <= 0.1*$category2occ{$category});
    next if ($category2type{$category}{$type} < 0.9*$category2knownocc{$category} && $category2knownocc{$category} <= 0.2*$category2occ{$category});
    print "$category\t$type\t$category2type{$category}{$type}/$category2knownocc{$category}/$category2occ{$category}\n";
    last;
  }
}
