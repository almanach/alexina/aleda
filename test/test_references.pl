#!/usr/bin/perl
use DBI;
use Encode;

binmode STDIN, ":utf8";
binmode STDOUT, ":utf8";
binmode STDERR, ":utf8";
use utf8;

my $oldstyle = 0;

my $lang = "fr";
my $dir = "..";

my $refs_dbh = DBI->connect("dbi:SQLite:$dir/ne_refs.$lang.dat", "", "", {RaiseError => 1, AutoCommit => 1});
my $refs_sth = $refs_dbh->prepare('select key from data where name=? and type=? limit 1;');
#my $refs_sth2 = $refs_dbh->prepare('select name,type from data where key=? limit 1;');

open DATA, "<test_references.data" || die $!;
binmode DATA, ":utf8";
while (<DATA>) {
  chomp;
  /^(.*?)\t(.*)$/;
  $data{$1} = $2;
}

for (keys %data) {
  /^(.*?) ### (.*)$/ || die;
  $name = $1;
  $type = $2;
  $refs_sth->execute($name, $type);
  while (my $id = $refs_sth->fetchrow) {
    print STDERR "MISMATCH: $_ is ".sprintf("%.0f", $id)." but was ".sprintf("%.0f", $data{$_})."\n" if (sprintf("%.0f", $id) ne sprintf("%.0f", $data{$_}));
  }
#  } elsif (/^\d+$/) {
#    $refs_sth2->execute($_);
#    while (my @row = $refs_sth2->fetchrow_array) {
#      print STDERR Encode::decode("utf8","$row[0] ### $row[1]")."\t".sprintf("%.0f", $_)."\n";
#    }
#  }
}
